({
    handleMenuClick: function (component, event, helper) {
        var itemData = component.get("v.itemData");
        HC.invokeEditRecord(
            "Case",
            itemData['Name'],
            itemData['Id']
        );

    }
})