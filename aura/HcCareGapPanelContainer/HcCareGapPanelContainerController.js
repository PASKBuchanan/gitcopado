({
    onSearchList: function(component, event, helper) {
        var searchText = component.find('caregapsearch-input').get('v.value');
        helper.typeAheadDelayExecute(function() {
            component.find('CareGapTableDataProvider').set('v.searchTerm', searchText);
        });
    }
})