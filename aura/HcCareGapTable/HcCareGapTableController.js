/* * Copyright © 2017 salesforce.com, inc. All rights reserved.
 * @copyright This document contains proprietary and confidential information and shall not be reproduced,
 * transferred, or disclosed to others, without the prior written consent of Salesforce.
 * @description Care Gap table controller function
 * @since 212.
 */
({
    onInit : function(component, event, helper) {
        var nameLinkFunction = function( itemData ) {
            var recordId = itemData[ 'Id' ];
            HC.openRecordSubTab(recordId, itemData[ 'Subject' ]);

        };

        var columnLinkFunctionMap = {
            'Subject' : nameLinkFunction
        };

        var columnLinkResolverFunctionMap = {
            Name : 'Subject'
        };
        component.set('v.columnLinkFunctionMap', columnLinkFunctionMap);
        component.set('v.columnLinkResolverFunctionMap', columnLinkResolverFunctionMap);
        component.set('v.showSpinner', true);
    },

    // Cleanup the following two methods. They are responsible for server-side search which we aren't using

    handleSearchChange: function(component, event, helper) {
        component.set("v.showSpinner",true);
        helper.getInitialData(component, event, helper);
    },

    handleLEXRecordChangedEvent: function (component, event, helper) {
        var record = event.getParam('record');
        //Check if the record change event is on Case Object
        if(!$A.util.isUndefinedOrNull(record) &&  record.sobjectType === 'Case'){
            //Check if the record change event is on Case Object of record type CareGap
            if(!$A.util.isUndefinedOrNull(record.RecordType)  && record.RecordType.Name === 'CareGap'){
                //Check if the record change event is for the right patient
                var patientId = component.get('v.patientId');
                if(record.AccountId == patientId || record.ContactId == patientId){
                    helper.getInitialData(component, event, helper); 
                }
            }
        }
    }
})