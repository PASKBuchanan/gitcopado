/** Copyright © 2015 salesforce.com, inc. All rights reserved.
 * @copyright This document contains proprietary and confidential information and shall not be reproduced,
 * transferred, or disclosed to others, without the prior written consent of Salesforce.
 * @description HcCarePlanBaseCmpontroller, js front-end controller for HcCarePlanBaseCmp component.
 * @since 198
 */
({
    doInit: function(component, event, helper) {
        component.set("v.showSpinner", true);       
        helper.getProblems(component);
        helper.setupProblemsCmp(component);
    },
    
    handleRefreshEvent: function(component, event, helper) {
        var carePlanId = component.get('v.carePlanId');
        var sObjectName = event.getParam('sObjectName').toUpperCase();
        var recordIdsToRefresh = event.getParam('recordIdsToRefresh');
        if (sObjectName == 'PROBLEM' && recordIdsToRefresh.indexOf(carePlanId) !== -1) {
            helper.getProblems(component);
        }
    },

    expandAllProblemsandGoals: function (component, event, helper) {
        var menuValue = event.getSource().get('v.value');
        var expandCollapseEvent = $A.get("e.HealthCloudGA:HcMultipleCarePlanEvent");
        if (menuValue == "ExpandAllProblemsandGoals") {
            expandCollapseEvent.setParams({
                "type": "Expand all problems and goals",
                "carePlanId" : component.get("v.carePlanId")
            });
        } else if (menuValue == "CollapseAllProblemsandGoals") {
            expandCollapseEvent.setParams({
                "type": "Collapse all problems and goals",
                "carePlanId" : component.get("v.carePlanId")
            });
        }
        expandCollapseEvent.fire();
        var problemsMenu = component.find("problemsMenu");
        problemsMenu.set("v.visible", false);
    }
})