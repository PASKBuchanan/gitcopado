/** Copyright © 2015 salesforce.com, inc. All rights reserved.
 * @copyright This document contains proprietary and confidential information and shall not be reproduced,
 * transferred, or disclosed to others, without the prior written consent of Salesforce.
 * @description HcCarePlanBaseCmpHelper, js front-end controller for HcCarePlanBaseCmp component.
 * @since 198
 */
({
    getCarePlanId: function(component) {
        var carePlanId = component.get("v.carePlanId");
        if ($A.util.isEmpty(carePlanId)) {
            var action = component.get("c.getCarePlanIdFromPatientId");
            action.setParams({
                "patientId": component.get("v.patientId")
            });
            action.setCallback(this, function(result) {
                if (result.getState() === "SUCCESS") {
                    carePlanId = result.getReturnValue();
                    component.set("v.carePlanId", carePlanId);
                }
                //Error from backend
                else {
                    throw new Error($A.get("$Label.HealthCloudGA.Msg_Error_General"));
                }
            });
            $A.enqueueAction(action);
        }

    },

    getProblems: function(component) {
        var self = this;
        var action;
        component.set("v.showSpinner", true);
        if (!$A.util.isUndefinedOrNull(component.get("v.carePlanId"))) {
            action = component.get("c.getProblemsForPlan");
            action.setParams({
                "carePlanId": component.get("v.carePlanId")
            });
        } else {
            action = component.get("c.getProblemsForPatient");
            action.setParams({
                "patientId": component.get("v.patientId")
            });
        }

        action.setBackground();
        action.setCallback(this, function(result) {
            var res = result.getReturnValue();
            var state = result.getState();
            var needCarePlanId = true;
            component.set('v.problemFetchDone', true);
            if (state === "SUCCESS") {
                if (!$A.util.isUndefinedOrNull(component.get("v.carePlanId"))) {
                    needCarePlanId = false;
                }
                if (res && res.length > 0 && needCarePlanId) {
                    var carePlanId = res[0].CarePlan__c;
                    component.set("v.carePlanId", carePlanId);
                }
                component.set("v.problems", res);
                if (needCarePlanId) {
                    self.getCarePlanId(component);
                }
            } else {
                self.handleError(component, result.getError());
            }

            component.set("v.showSpinner", false);

        });
        $A.enqueueAction(action);
    },

    setupProblemsCmp: function(component) {
        $A.createComponent("HealthCloudGA:HcCarePlanBaseProblemsCmp", {
                "aura:id": "problemsCmp",
                "patientId": component.getReference("v.patientId"),
                "carePlanId": component.getReference("v.carePlanId"),
                "problems": component.getReference("v.problems"),
                "problemFetchDone": component.getReference("v.problemFetchDone")
            },
            function(newCmp, status, errorMessage) {
                if (status === "SUCCESS") {
                    var problemCmp = component.find("carePlanTab");
                    problemCmp.set("v.body", newCmp);
                } else if (status === "INCOMPLETE") {
                    console.error("No response from server or client is offline.")
                        // Show offline error
                } else if (status === "ERROR") {
                    console.error("Error: " + errorMessage);
                    // Show error message
                }
                component.set("v.showSpinner", false);
            }
        );
    }
})