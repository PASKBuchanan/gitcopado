({
    handleTaskEvent: function(component, event, helper) {
        helper.raiseTaskSObjectEvent(component, event.getParams());
        event.stopPropagation();
    },

    handleCarePlanEvent: function(component, event, helper) {
        helper.handleCarePlanEvent(component, event, helper);
    },

    handleRefreshEvent: function(component, event, helper) {
        var goalId = component.get('v.goal.Id');
        var sObjectName = event.getParam('sObjectName').toUpperCase();
        var recordIdsToRefresh = event.getParam('recordIdsToRefresh');
        if (sObjectName === 'TASK' && recordIdsToRefresh.indexOf(goalId) !== -1) {
            helper.refresh(component);
        }
    },
    
    expandCollapseAllGoals: function(component, event, helper) {
        var type = event.getParam("type");
        var carePlanId = event.getParam("carePlanId");
        var expanded = component.get('v.expanded');
        if (!expanded && (type === "Expand all problems and goals" || type === "Expand all goals") && carePlanId == component.get("v.carePlanId") ||
                expanded && (type === "Collapse all problems and goals" || type === "Collapse all goals")  && carePlanId == component.get("v.carePlanId")) {
            var goalHeader = component.find("goalHeader");
            goalHeader.expand();
        }
    }
})