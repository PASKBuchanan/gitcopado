/** Copyright © 2015 salesforce.com, inc. All rights reserved.
 * @copyright This document contains proprietary and confidential information and shall not be reproduced,
 * transferred, or disclosed to others, without the prior written consent of Salesforce.
 * @description HcCarePlanHeaderController, js front-end controller for HcCarePlanHeader component.
 * @since 198
 */
({
    doInit: function(component, event, helper) {
        if (component.get("v.showActionMenu")) {
            helper.createActionMenu(component);
        }
    },

    handleAction: function(component, event, helper) {
        helper.raiseActionMenuEvent(component, event.getParam('value'));
    },

    expand: function(component, event, helper) {
        event.stopPropagation();
        component.set('v.expanded', !component.get('v.expanded'));
        /*
         * Generate Component Event indicating Expansion
         */
        var expandEvent = component.getEvent("HcCarePlanEvent");
        expandEvent.setParams({
            "type": "EXPAND"
        });
        expandEvent.fire();
    },

    handleRefreshEvent: function(component, event, helper) {
        var carePlanItemId = component.get('v.carePlanItem.Id');
        
        if(!$A.util.isEmpty(carePlanItemId)) {
            var sObjectName = event.getParam('sObjectName').toUpperCase();
            var headerType = component.get('v.headerType');
            var recordIdsToRefresh = event.getParam('recordIdsToRefresh');
            if(recordIdsToRefresh.indexOf(carePlanItemId) !== -1) {
                if(sObjectName === headerType) {
                    helper.updateHeaderDetails(component, event.getParam('record'));
                }
            }
        }
    },

    carePlanHeaderCheckBoxMethod: function(component, event, helper) {
        helper.carePlanHeaderCheckBoxMethod(component, event);
    },

    checkBoxClicked: function(component, event, helper) {
        event.stopPropagation();
        var checkboxEvent = component.getEvent("HcCarePlanHeaderCheckBoxEvent");
        if (checkboxEvent != null) {
            var checked = component.find("checkbox").get("v.value");
            /*
             * Generate Component Event indicating that checkbox was clicked
             */
            checkboxEvent.setParams({
                "checked": checked,
            });
            checkboxEvent.fire();
        }
    },

})