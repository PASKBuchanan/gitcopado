/** Copyright © 2015 salesforce.com, inc. All rights reserved.
 * @copyright This document contains proprietary and confidential information and shall not be reproduced,
 * transferred, or disclosed to others, without the prior written consent of Salesforce.
 * @description HcCarePlanHeaderHelper, js front-end helper for HcCarePlanHeader component.
 * @since 198
 */
({
    menuItemsMap: {
        PROBLEM: [
            {
                labelId: '$Label.HealthCloudGA.Menu_Item_Create_Goal',
                value: function(component) {
                    return {
                        sObjectAction: "CREATE",
                        sObjectType: "Goal",
                        sObject: {
                            CarePlanProblem__c: component.get('v.carePlanItem.Id')
                        }
                    };
                }
            },
            {
                labelId: '$Label.HealthCloudGA.Menu_Item_Edit_Problem',
                value: function(component) {
                    return {
                        sObjectAction: "EDIT",
                        sObjectType: "Problem",
                        sObject: {
                            Id: component.get('v.carePlanItem.Id'),
                            CarePlan__c: component.get('v.carePlanId')
                        }
                    };
                }
            }
        ],
        GOAL: [
            {
                labelId: '$Label.HealthCloudGA.Link_Create_Task',
                value: function(component) {
                    return {
                        fromWhere: "CarePlan",
                        sObjectAction: "CREATE",
                        sObjectType: "Task",
                        sObject: {
                            CarePlanProblem__c: component.get('v.carePlanItem.CarePlanProblem__c'),
                            CarePlanGoal__c: component.get('v.carePlanItem.Id'),
                            WhatId: component.get('v.carePlanId')
                        }
                    };
                }                   
            },
            {
                labelId: '$Label.HealthCloudGA.Menu_Item_Edit_Goal',
                value: function(component) {
                    return {
                        sObjectAction: "EDIT",
                        sObjectType: "Goal",
                        sObject: {
                            Id: component.get('v.carePlanItem.Id'),
                            CarePlanProblem__c: component.get('v.carePlanItem.CarePlanProblem__c'),
                            CarePlan__c: component.get('v.carePlanId')
                        }
                    };
                }
            }
        ] 
    },

    createActionMenu: function(component) {
        var self = this;
        var generateMenuItems = function(menuItemKey) {
            var menuItemSpecs = [];
            if(self.menuItemsMap.hasOwnProperty(menuItemKey)) {
                var menuItemsSpec = self.menuItemsMap[menuItemKey];
                for(let ii=0; ii<menuItemsSpec.length; ii++) {
                    menuItemsSpec[ii].label = $A.get(menuItemsSpec[ii].labelId);
                    menuItemSpecs.push([
                        "lightning:menuItem",
                        menuItemsSpec[ii]
                    ]);                    
                }
            }
            return menuItemSpecs;
        };

        var menuItemsToCreate = generateMenuItems(component.get('v.headerType'));

        $A.createComponents(menuItemsToCreate, function(menuItems, status, statusMessage) {
            if (status == "SUCCESS") {
                component.set("v.actionMenuItems", menuItems);
            }
        });
    },

    raiseActionMenuEvent: function(component, valueFunction) {
        if( typeof valueFunction == "function" ) {
            var sObjectEvent = component.getEvent("sObjectEvent");
            sObjectEvent.setParams(valueFunction(component));
            sObjectEvent.fire();
        }
    },

    updateHeaderDetails: function(component, sObjectRecord) {
        var item = component.get('v.carePlanItem');
        for(let fieldName in sObjectRecord) {
            if(item.hasOwnProperty(fieldName)) {
                item[fieldName] = sObjectRecord[fieldName];
            }
        }
        component.set('v.carePlanItem', item);
    },

    carePlanHeaderCheckBoxMethod: function(component, event) {
        //To do - Do this only when checkBoxSelectedChanged
        var params = event.getParam('arguments');
        var checked = false;
        if (params) {
            checked = params.checked;
        }

        component.find("checkbox").set("v.value", checked);
    }
})