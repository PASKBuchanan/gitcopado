({
    onInit: function (component, event, helper) {
        //This is for single care plan context.
        if (component.get("v.expanded")) {
            helper.setExpandedTable(component.find('panelRow'), component);
            component.set("v.expandIconClass", "slds-hide");
        }
        var carePlanData = component.get('v.itemData');
	    // Set Patient ID for Care Plan Panel when listed under Multiple Care Plans view
        if($A.util.isEmpty(component.get('v.patientId'))) {
            if(!$A.util.isUndefinedOrNull(carePlanData) &&
                !$A.util.isUndefinedOrNull(carePlanData.row) && 
                !$A.util.isUndefinedOrNull(carePlanData.row.patientId)) {
                component.set('v.patientId', carePlanData.row.patientId);
            } else {
                console.error('HcCarePlanPanel : Patient ID not set');
            }
        }
    },

    toggleIcon: function (component, event, helper) {
        helper.togglePanel(component, event);    
    },
    
    expandCollapseCarePlans: function (component, event, helper) {
        if (event.getParam("allCarePlansTabId") === component.get("v.itemData").allCarePlansTabId) {
            helper.expandCollapseCarePlans(component, event);
        }
    },

    handleSObjectEvent: function(component, event, helper) {
        var sObjectName = event.getParam('sObjectType').toUpperCase();
        var sObjectRecord = event.getParam('sObject') || {};

        helper.updateRecordRefreshRegistry(component, sObjectName, sObjectRecord);

        var sObjectModalMap = {
            TASK: helper.openTaskModal,
            GOAL: helper.openGoalModal,
            PROBLEM: helper.openProblemModal
        }
        if(sObjectModalMap.hasOwnProperty(sObjectName)) {
            // Using a singleton variable to store component's global ID, so that 
            // only the Care Plan Panel component that invokes the modal 
            // handles the application event after a modal is closed 
            helper.modalInvokerComponentId = component.getGlobalId();
            sObjectModalMap[sObjectName](component, event);
        }
    },

    handleLEXRecordChangedEvent: function(component, event, helper) {
        var record = helper.stripNamespaceFromRecord(event.getParam('record'));
        var sObjectType = record.sobjectType;
        
        // Ensures this component handles only application events arising from modals opened via HcSObjectEvent
        if(helper.modalInvokerComponentId === component.getGlobalId()) {
            // Safeguard against customer layout not having CarePlan__c field
            // we assume it is associated with current Care Plan
            if(!record.hasOwnProperty('CarePlan__c')) {
                record.CarePlan__c = component.get("v.itemData").row.Id;
            }
            helper.modalInvokerComponentId = '';
            helper.raiseRefreshEvent(component, sObjectType, record);
        }
    },

    handleDataChangedEvent: function(component, event, helper) {
        var record = helper.stripNamespaceFromRecord(event.getParam("record"));
        var sObjectType = event.getParam("sObjectType");

        // Ensures this component handles only application events arising from modals opened via HcSObjectEvent
        if(helper.modalInvokerComponentId === component.getGlobalId()) {
            // HcFieldSetModal does not allow editing Care Plan 
            // so if CarePlan__c field is not available in the incoming record
            // we assume it is associated with current Care Plan
            if(!record.hasOwnProperty('CarePlan__c')) {
                record.CarePlan__c = component.get("v.itemData").row.Id;
            }

            helper.modalInvokerComponentId = '';
            helper.raiseRefreshEvent(component, sObjectType, record);
        }
    },

    handleComponentStatusEvent: function(component, event, helper) {
        var memberObj = event.getParam("memberObj");
        var carePlanId = component.get("v.itemData").row.Id;
        // TODO: Check if topLevelParentId (across components) is really needed and remove if not.
        if (memberObj != null && (memberObj.carePlanId == carePlanId || memberObj.topLevelParentId == carePlanId)) {
            helper.handleMessageEvent(component, event);
        }
    }
})