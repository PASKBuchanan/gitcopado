({
    fieldsToMonitorMap: {
        TASK: ['WhatId', 'CarePlanProblem__c', 'CarePlanGoal__c', 'OwnerId'],
        GOAL: ['CarePlan__c', 'CarePlanProblem__c'],
        PROBLEM: ['CarePlan__c']
    },

    updateRecordRefreshRegistry: function(component, sObjectName, sObjectRecord) {
        var recordRefreshRegistry = component.get('v.recordRefreshRegistry');

        var fieldsToMonitor = this.fieldsToMonitorMap[sObjectName];
        if (!$A.util.isUndefinedOrNull(sObjectRecord.Id) && !$A.util.isUndefinedOrNull(fieldsToMonitor)) {
            var refreshRegistryEntry = {};
            for (let ii = 0; ii < fieldsToMonitor.length; ii++) {
                refreshRegistryEntry[fieldsToMonitor[ii]] = sObjectRecord.hasOwnProperty(fieldsToMonitor[ii]) ? sObjectRecord[fieldsToMonitor[ii]] : '';
            }
            recordRefreshRegistry[sObjectRecord.Id] = refreshRegistryEntry;
        }

        component.set('v.recordRefreshRegistry', recordRefreshRegistry);
    },

    setExpandedTable: function(cmp, component) {
        var self = this;
        if ($A.util.isUndefinedOrNull(component.get("v.itemData").row)) {
            this.showToast(component, { message: $A.get("$Label.HealthCloudGA.Msg_Error_Unable_To_Retrive_CarePlan") }, true, 'warning');
        } else {
            var patientAccountId = component.get("v.itemData").row.AccountId;
            var carePlanId = component.get("v.itemData").row.Id;
            var Subject = component.get("v.itemData").row.Subject;
            component.set("v.showSpinner", true);
            var action = component.get("c.getTabSettings");
            action.setStorable();
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    if (result.length > 0) {
                        $A.createComponent("HealthCloudGA:HcFlexibleTabs", { data: result, patientId: patientAccountId, carePlanId: carePlanId, carePlanSubject: Subject }, function(newTable, status, statusMessage) {
                            if (status === "SUCCESS") {
                                cmp.set("v.body", newTable);
                                component.set("v.showSpinner", false);
                            }
                        });
                    } else {
                        component.set("v.showSpinner", false);
                        self.showToast(component, { message: $A.get("$Label.HealthCloudGA.Label_FCP_No_Tabs") }, true, 'error');
                    }
                }
                if (state === "ERROR") {
                    this.handleError(component, action.getError());
                    component.set("v.showSpinner", false);
                }
                component.set("v.showSpinner", false);
            });
            $A.enqueueAction(action);

        }
    },

    togglePanel: function(component, event) {
        var expanded = component.get("v.expanded");
        var firstTimeAccessed = component.get("v.firstTimeAccessed");
        component.set("v.expanded", !expanded);
        var tableRow = component.find('panelRow');
        if (firstTimeAccessed) {
            //if first time, not cached. Run spinner
            this.setExpandedTable(tableRow, component);
        }
        component.set("v.firstTimeAccessed", false);
    },

    expandCollapseCarePlans: function(component, event) {
        var expanded = component.get("v.expanded");
        if (event.getParam("type") === "Expand all Care Plans") {
            if (!expanded) {
                this.togglePanel(component, event);
                component.set("v.expanded", !expanded);
            }
        } else if (event.getParam("type") === "Collapse all Care Plans") {
            if (expanded) {
                this.togglePanel(component, event);
                component.set("v.expanded", !expanded);
            }
        }
    },

    openTaskModal: function(component, event) {
        var carePlanId = component.get("v.itemData").row.Id;
        var fromWhere = event.getParam("fromWhere");
        var modalActionType = event.getParam("sObjectAction");

        var taskModalAttributes = {
            fromWhere: $A.util.isUndefinedOrNull(fromWhere) ? "CarePlan" : fromWhere,
            carePlanId: carePlanId,
            AssignedTo: HC.convert15to18DigitId($A.get("$SObjectType.CurrentUser.Id")),
            userDateFormat: $A.get("$Locale.dateFormat"),
            userDateTimeFormat: $A.get("$Locale.datetimeFormat"),
            lookUpWhereClauseMap: {
                CarePlan__c: carePlanId
            }
        };

        var taskRecord = event.getParam("sObject");
        if (!$A.util.isUndefinedOrNull(taskRecord)) {

            taskModalAttributes.problem = $A.util.isEmpty(taskRecord.CarePlanProblem__c) ? '' : taskRecord.CarePlanProblem__c;

            var goalId = taskRecord.CarePlanGoal__c;
            if (!$A.util.isEmpty(goalId)) {
                taskModalAttributes.selectedGoal = goalId;
            }

            if (!$A.util.isEmpty(taskRecord.OwnerId)) {
                taskModalAttributes.AssignedTo = taskRecord.OwnerId;
            }

            if (modalActionType == "EDIT") {
                taskModalAttributes.SObjectId = taskRecord.Id;
                taskModalAttributes.newTask = taskRecord;
                taskModalAttributes.action = "UpdateTask";
                taskModalAttributes.PerformedBy = taskRecord.WhoId;
                taskModalAttributes.RelatedTo = taskRecord.WhatId;
            }
        }

        var modalCreationCallback = function(view) {
            component.set("v.modalCmp", view);
        };

        if (modalActionType == "EDIT") {
            HC.invokeEditRecord('Task', null, taskRecord.Id, taskModalAttributes, modalCreationCallback);
        } else {
            HC.invokeCreateRecord('Task', null, taskRecord, taskModalAttributes, modalCreationCallback);
        }
    },

    openGoalModal: function(component, event) {
        var carePlanId = component.get("v.itemData").row.Id;
        var modalActionType = event.getParam("sObjectAction");

        var goalRecord = event.getParam('sObject');
        var problemId = $A.util.isUndefinedOrNull(goalRecord) ? "" : goalRecord.CarePlanProblem__c;

        var modalAttributes = {
            columnSize: "1",
            fieldSetName: "GoalDefaultFieldSet",
            auraId: "MandatorySection",
            viewType: "Edit",
            topLevelParentId: carePlanId
        };

        var modalCreationCallback = function(view) {
            if (view.isValid() && component.isValid()) {
                component.set("v.modalCmp", view);
            }
        };

        var defaultValuesMap = {};
        // Checks needed to avoid supplying empty strings to one.app events
        if (!$A.util.isEmpty(carePlanId)) {
            defaultValuesMap.CarePlan__c = carePlanId;
        }
        if (!$A.util.isEmpty(problemId)) {
            defaultValuesMap.CarePlanProblem__c = problemId;
        }

        if (modalActionType === "CREATE") {
            HC.invokeCreateRecord(
                'CarePlanGoal__c',
                $A.get("$Label.HealthCloudGA.Header_New_Goal"),
                defaultValuesMap,
                modalAttributes,
                modalCreationCallback
            );
        } else if (modalActionType === "EDIT") {
            if (!$A.util.isUndefinedOrNull(goalRecord)) {
                HC.invokeEditRecord(
                    'CarePlanGoal__c',
                    $A.get("$Label.HealthCloudGA.Menu_Item_Edit_Goal"),
                    goalRecord.Id,
                    modalAttributes,
                    modalCreationCallback
                );
            }
        }
    },

    openProblemModal: function(component, event) {
        var carePlanId = component.get("v.itemData").row.Id;
        var patientAccountId = component.get("v.itemData").row.AccountId;
        var modalActionType = event.getParam("sObjectAction");

        var modalAttributes = {
            columnSize: "1",
            fieldSetName: "ProblemDefaultFieldSet",
            auraId: "MandatorySection",
            viewType: "Edit",
            topLevelParentId: carePlanId
        };

        var modalCreationCallback = function(view) {
            if (view.isValid() && component.isValid()) {
                component.set("v.modalCmp", view);
            }
        };

        var defaultValuesMap = {};
        // Checks needed to avoid supplying empty strings to one.app events
        if (!$A.util.isEmpty(carePlanId)) {
            defaultValuesMap.CarePlan__c = carePlanId;
        }
        if (!$A.util.isEmpty(patientAccountId)) {
            defaultValuesMap.Account__c = patientAccountId;
        }

        if (modalActionType === "CREATE") {
            HC.invokeCreateRecord(
                'CarePlanProblem__c',
                $A.get("$Label.HealthCloudGA.Header_New_Problem"),
                defaultValuesMap,
                modalAttributes,
                modalCreationCallback
            );
        } else if (modalActionType === "EDIT") {
            var problemRecord = event.getParam("sObject");
            if (!$A.util.isUndefinedOrNull(problemRecord)) {
                HC.invokeEditRecord(
                    'CarePlanProblem__c',
                    $A.get("$Label.HealthCloudGA.Menu_Item_Edit_Problem"),
                    problemRecord.Id,
                    modalAttributes,
                    modalCreationCallback
                );
            }
        }
    },

    getRecordIdsToRefresh: function(component, sObjectName, updatedRecord) {
        var recordRefreshRegistry = component.get('v.recordRefreshRegistry');
        var monitoredFields = this.fieldsToMonitorMap[sObjectName];
        var idsToRefresh = $A.util.isEmpty(updatedRecord.Id) ? [] : [updatedRecord.Id];

        for (let ii = 0; ii < monitoredFields.length; ii++) {
            let updatedFieldValue = updatedRecord[monitoredFields[ii]];
            if ($A.util.isUndefinedOrNull(updatedRecord.Id) || !recordRefreshRegistry.hasOwnProperty(updatedRecord.Id)) {
                // Add all non-empty id field values from updatedRecord for 
                // fields in fieldsToMonitorMap for specified sObjectName for newly created record
                if (!$A.util.isEmpty(updatedFieldValue)) {
                    idsToRefresh.push(updatedFieldValue);
                }
            } else {
                // If updatedRecord's Id exists in recordRefreshRegistry 
                // compare id fields values with stored record and add to list if they differ
                let originalFieldValue = recordRefreshRegistry[updatedRecord.Id][monitoredFields[ii]];
                if (!$A.util.isEmpty(originalFieldValue)) {
                    idsToRefresh.push(originalFieldValue);
                }
                if (originalFieldValue !== updatedFieldValue) {
                    if (!$A.util.isEmpty(updatedFieldValue)) {
                        idsToRefresh.push(updatedFieldValue)
                    }
                }
            }
        }

        // Delete recordId entry and associated field data 
        // from refresh registry after retrieving ids to refresh
        delete recordRefreshRegistry[updatedRecord.Id];
        component.set('v.recordRefreshRegistry', recordRefreshRegistry);

        return idsToRefresh;
    },

    raiseRefreshEvent: function(component, sObjectType, record) {
        var sObjectApiNameMap = {
            Task: 'TASK',
            CarePlanGoal__c: 'GOAL',
            CarePlanProblem__c: 'PROBLEM'
        };

        if (sObjectApiNameMap.hasOwnProperty(sObjectType)) {
            var idsToRefresh = this.getRecordIdsToRefresh(component, sObjectApiNameMap[sObjectType], record);

            var refreshEvent = $A.get("e.HealthCloudGA:HcCarePlanRefreshEvent");
            refreshEvent.setParams({
                recordIdsToRefresh: idsToRefresh,
                sObjectName: sObjectApiNameMap[sObjectType],
                record: record
            });
            refreshEvent.fire();
        }
    },

    //TODO - Siva: Refine
    handleMessageEvent: function(component, event) {
        var status = event.getParam('status');
        if(status !== 'ERROR') {
            component.set('v.modalCmp', []);  
        }
        if (status === 'CANCELLED' || $A.util.isUndefinedOrNull(status)) {
            return;
        }

        var messageBody = {
            message: event.getParam('message')
        };
        this.showToast(component, messageBody, true, status.toLowerCase());
    }
})