({
    getProblemGoals: function (component, event, helper) {
        helper.getGoals(component, event, helper);
    },

    handleCarePlanEvent: function (component, event, helper) {
        helper.handleCarePlanEvent(component, event, helper);
    },

    handleRefreshEvent: function(component, event, helper) {
        var problemId = component.get('v.problem.Id');
        var sObjectName = event.getParam('sObjectName').toUpperCase();
        var recordIdsToRefresh = event.getParam('recordIdsToRefresh');
        if (sObjectName === 'GOAL' && recordIdsToRefresh.indexOf(problemId) !== -1) {
            helper.refresh(component);
        }
    },

    expandCollapseAllProblems: function (component, event, helper) {
        var type = event.getParam("type");
        var carePlanId = event.getParam("carePlanId");
        var expanded = component.get('v.expanded');
        if (!expanded && type === "Expand all problems and goals" && carePlanId == component.get("v.carePlanId") ||
            expanded && type === "Collapse all problems and goals" && carePlanId == component.get("v.carePlanId")) {
            component.set("v.expandGoals", true);
            var cmpHeader = component.find("problemHeader");
            cmpHeader.expand();
        }
    }
})