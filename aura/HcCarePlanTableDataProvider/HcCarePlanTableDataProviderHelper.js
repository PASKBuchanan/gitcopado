({
    getData: function(actionName, component, event, helper, columnMeta) {

        var patientId = component.get("v.patientId");
        if($A.util.isEmpty(actionName)) {
            actionName = 'getCarePlanDetailsByPatient';
        }

        var actionMap = {
            getCarePlanDetailsByPatient : "c.getCarePlanDetailsByPatient"
        };

        var stateTabIdMap = {
            open: 'openCases',
            closed: 'closedCases',
            all: 'allCases'
        };
        var state = component.get("v.state");
        var allCarePlansTabId = stateTabIdMap.hasOwnProperty(state) ? stateTabIdMap[state] : 'allCases';

        var actionParamsMap = {
            getCarePlanDetailsByPatient : {
                "patientId": patientId,
                'state' : state,
                'retrieveProblems' : false
            }
        };

        var action = helper.getAction(actionName,actionMap,actionParamsMap,component,columnMeta);
        if( $A.util.isEmpty(action)) {
            var errorMsg = $A.get("$Label.HealthCloudGA.Msg_Invalid_Filter_Message");
            var compEvent = component.getEvent("error");
            compEvent.setParams({error:errorMsg});
            compEvent.fire();
            return;
        }

        var xhrStart = new Date().getTime();
        action.setCallback(this, function(response) {
            var xhrEnd = new Date().getTime();
            helper.progressMessage(' Xhr: '+ actionName +'#' + (xhrEnd - xhrStart),component.get('v.startT'));
            if (response.getState() == "SUCCESS") {
                var result = response.getReturnValue();
                var _resultList = result.recordsData;

                var processResult = helper.processResponsePageControl(component,columnMeta,result);
                
                var panelMeta = [];
                var panelRow = [];
                for(var i=0;i<_resultList.length;i++) {
                    _resultList[i].patientId = patientId;
                    var resultRow = {
                        metadata: processResult.colMetadata,
                        row:_resultList[i],
                        Id: _resultList[i].Id,
                        allCarePlansTabId : allCarePlansTabId
                    };
                    panelRow.push(resultRow);
                }
                
                helper.raiseDataFetchedEvent(component,
                                             processResult.eventType,
                                             panelMeta,
                                             panelRow,
                                             processResult.hasMoreData,
                                             actionName);
            } else if (response.getState() === "ERROR") {
                // add exception handling
                var errorMsg = helper.getErrorMessage(response);
                if(errorMsg.indexOf($A.get("$Label.HealthCloudGA.Msg_Component_Has_NoAccess"))!=-1){
                   component.set("v.isPSLEnforced", true);
               }else{
                var compEvent = component.getEvent("error");
                compEvent.setParams({error:errorMsg});
                compEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
})