/* Copyright © 2015 salesforce.com, inc. All rights reserved.
 * @copyright This document contains proprietary and confidential information and shall not be reproduced,
 * transferred, or disclosed to others, without the prior written consent of Salesforce.
 * @description HcCarePlanTemplateContainerController.
 * @since 204
 */
({
    onScriptsLoaded: function(component, event, helper) {
        component.set('v.scriptsLoaded', true);
        helper.init(component);
    },

    onInit: function(component, event, helper) {
        helper.init(component);
    },

    toggleSpinner: function(component, event, helper) {
        helper.toggleSpinner(event.getParam("isVisible"), component);
    },

    handleNextClickOnTab: function(component, event, helper) {
        var currentTabName = helper.getCurrentTabId(component);
        helper.closeToast(component);
        component.set("v.showDueDateCareCoordinator", false);
        if (currentTabName == "selectTemplateTab") {
            helper.handleNextClickOnSelectTemplateTab(component);
        } else if (currentTabName == "customizeToPatientTab") {
            helper.handleNextClickOnCustomizeToPatientTab(component);
        } else {
            helper.showToast(component, { 'message': $A.get("$Label.HealthCloudGA.Msg_Error_General") }, true, 'error');
        }
    },

    handleBackClickOnTab: function(component, event, helper) {
        var currentTabName = helper.getCurrentTabId(component);
        helper.closeToast(component);
        component.set("v.showDueDateCareCoordinator", false);
        if (currentTabName == "customizeToPatientTab") {
            component.set("v.showAlert", true);
        } else if (currentTabName == "reviewAndApplyTab") {
            component.set("v.showDueDateCareCoordinator", true);
            helper.handleBackClickOnReviewAndApplyTab(component);
        } else {
            helper.showToast(component, { 'message': $A.get("$Label.HealthCloudGA.Msg_Error_General") }, true, 'error');
        }
    },

    handleApplyClickOnTab: function(component, event, helper) {
        helper.handleApplyClickOnTab(component);
    },
    handlePromptOkClick: function(component, event, helper) {
        component.set("v.showAlert", false);
        helper.handleBackClickOnCustomizeToPatientTab(component);
    },
    handlePromptCancelClick: function(component, event, helper) {
        component.set("v.showAlert", false);
    },
    processAfterApply: function(component, event, helper) {
        helper.processAfterApply(component, event);
    }
})