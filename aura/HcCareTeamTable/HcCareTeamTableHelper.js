({
    showModalComponent: function( component, innerComponentMarkup, attributes ) {
        $A.createComponent(innerComponentMarkup, attributes,
            function( view, status, errorMessage ) {
                component.set("v.modal", view);
            }
        );
    },

    resetModalComponent: function( component ) {
        var modal = component.get('v.modal');
        if (modal != undefined && modal.length > 0) {
            component.set('v.modal', []);
        }
    },

    actionMap : {

        EditMember : function( component, memberData ) {
            HC.invokeEditRecord( null, memberData.name, memberData.memberId );
        },

        AddMemberToCommunity : function( component, memberData, thisHelper ) {
            var attributes = { memberObj: memberData };
            thisHelper.showModalComponent( component, "HealthCloudGA:HcCareTeamEnableCommCmp", attributes );
        },

        PrivateMessage : function( component, memberData, thisHelper ) {
            var attributes = { memberObj: memberData };
            thisHelper.showModalComponent( component, "HealthCloudGA:HcCareTeamDirectMessageCmp", attributes );
        },

        CreateTask : function( component, memberData, thisHelper ) {
            var sObjectEventParams = {
                sObjectAction: "CREATE",
                sObjectType: "Task",
                sObject: {
                    OwnerId: memberData.memberId || "",
                    WhatId: component.get('v.carePlanId')
                }
            };

            var sObjectEvent = component.getEvent("sObjectEvent");
            sObjectEvent.setParams(sObjectEventParams);
            sObjectEvent.fire();
        },

        RemoveMember : function( component, memberData, thisHelper ) {
            var attributes = { memberObj: memberData };
            thisHelper.showModalComponent( component, "HealthCloudGA:HcCareTeamRemoveModalCmp", attributes );
        }

    },

	handleCareTeamTableAction : function( component, actionType, memberData ) {
	   if( this.actionMap.hasOwnProperty( actionType ) ) {
            this.actionMap[ actionType ]( component, memberData, this );
       }	
	},

    modalEventMap : {

        REMOVEMODAL : function( component, thisHelper ) {
            thisHelper.resetModalComponent( component );
            thisHelper.fetchData( component );
        },

        CANCELMODAL : function( component, thisHelper ) {
            thisHelper.resetModalComponent( component );
        }
    },

    handleCareTeamModalEvent: function( component, event ) {
        var eventType = event.getParam("type");
        if( this.modalEventMap.hasOwnProperty( eventType ) ) {
            this.modalEventMap[ eventType ]( component, this );
        }
    }
})