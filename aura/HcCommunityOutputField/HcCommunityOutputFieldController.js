({
	onInit : function(component, event, helper) {
		helper.initData(component, event, helper);
	},

    updateStatus: function(component, event, helper){
        helper.updateStatus(component, event, helper);
    }
})