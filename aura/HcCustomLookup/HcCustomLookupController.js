/** Copyright © 2015 salesforce.com, inc. All rights reserved.
 * @copyright This document contains proprietary and confidential information and shall not be reproduced,
 * transferred, or disclosed to others, without the prior written consent of Salesforce.
 * @description HcCustomLookupController, js front-end controller for HcCustomLookup component.
 * @since 198
 */
({
    doInit: function(component, event, helper) {
        var recId = component.get("v.selectedId");
        var sObjectName = component.get("v.Sobject");
        if (recId != 'null' && recId != '' && recId != $A.get("$Label.HealthCloudGA.Text_Lookup_NoResults") && !$A.util.isEmpty(sObjectName) ) {
            helper.retrieveData(component);
        }
    },

    searchRecords: function(component, event, helper) {
        var lookupValue = event.getSource().get('v.value').trim();
        helper.typeAheadDelayExecute(function() {
            helper.searchRecords(component, lookupValue);       
        });        
    },

    searchRecordsOnKeyUp: function(component, event, helper) {
        var lookupValue = component.find("lookupValue").get('v.value').trim();
        if (helper.isEnterKeyPressed(event)) {
            helper.searchRecords(component, lookupValue);
        }
    },

    toggleLookupList : function(component, event, helper) {
        if(event.type === "blur") {
            component.set("v.renderResult", false);
        }
    },
    
    processSelected: function(component, event, helper) {
        var theTarget = event.srcElement || event.target;
        var selId = HC.getDataAttribute(theTarget, "role");
        component.set("v.renderResult", false);
        if (!$A.util.isUndefinedOrNull(selId)) {
            component.set("v.selectedId", selId);
        }
    }
})