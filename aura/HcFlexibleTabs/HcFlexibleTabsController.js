({
    onInit: function(component, event, helper) {
        component.set('v.showSpinner', true);
        var data = component.get("v.data");
        var count = 0;
        for (var i = 0; i < data.length; i++) {
            $A.get(HC.getLabelIdFromUserText(data[i].label), (function(i) {
                return function(label) {
                    if (!$A.util.isUndefinedOrNull(label) && label.indexOf('$Label.') == -1) {
                        data[i].label = label;
                    }
                    count++;
                    if (count === data.length) {
                        component.set("v.initDone", true);
                        component.set("v.data", data);
                        component.set("v.defaultTabId", data[0].componentName);
                        component.set('v.showSpinner', false);
                    }
                };
            })(i));
        }
    },

    handleActive: function(component, event, helper) {
        helper.handleActive(component, event);
    },

    createSObject: function(component, event, helper) {
        var sObjectKey = event.getSource().get('v.value').toUpperCase();
        var sObjectEventParams = {
            fromWhere: 'FlexibleCarePlan',
            sObjectAction: 'CREATE',
            sObjectType: sObjectKey
        };
        if(sObjectKey == 'TASK') {
            sObjectEventParams.sObject = {
                WhatId: component.get("v.carePlanId")
            }
        }
        helper.raiseSObjectEvent(component, sObjectEventParams);           
    },

    handleApplyCarePlanTemplates: function(component, event, helper) {
        helper.actionMenuFunctionMap['ApplyCarePlanTemplates'](component);
    },

    expandAllProblemsandGoals: function(component, event, helper) {
        var menuValue = event.getSource().get('v.value');
        var expandCollapseEvent = $A.get("e.HealthCloudGA:HcMultipleCarePlanEvent");
        if (menuValue == "ExpandAllProblemsandGoals") {
            expandCollapseEvent.setParams({
                "type": "Expand all problems and goals",
                "carePlanId": component.get("v.carePlanId")
            });
        } else if (menuValue == "CollapseAllProblemsandGoals") {
            expandCollapseEvent.setParams({
                "type": "Collapse all problems and goals",
                "carePlanId": component.get("v.carePlanId")
            });
        }
        expandCollapseEvent.fire();
    },

    openCollaboration: function(component, event, helper) {
        var tabName = $A.get("$Label.HealthCloudGA.Collaboration");
        var carePlanSubject = component.get("v.carePlanSubject");
        if (!$A.util.isEmpty(carePlanSubject)) {
            tabName = tabName + ':' + carePlanSubject;
        }
        var patientId = component.get("v.patientId");
        var carePlanId = component.get("v.carePlanId");
        var isSubtabOpened = HC.openCaseTeamSubTab(patientId, carePlanId, tabName, true);
        component.set('v.isCollaborationSubTabOpened', isSubtabOpened);
    },

    handleAddCareTeamMember: function(component, event, helper) {
        helper.actionMenuFunctionMap['AddCareTeamMember'](component);
    },

    handleAddCareTeamMemberEvent: function(component, event, helper) {
        if (event.getParam("type") === "REMOVEMODAL") { // On close of add care team member modal
            helper.resetModalComponent(component);
            var refreshEvent = $A.get("e.HealthCloudGA:HcComponentStatusEvent");
            var carePlanId = component.get("v.carePlanId");
            refreshEvent.setParams({
                type: "REFRESH_CARETEAMLIST",
                memberObj: {
                    carePlanId: carePlanId
                }
            });
            refreshEvent.fire();
        }
    },

    handleActionMenuEvent: function(component, event, helper) {
        var menuItemVaue = event.getSource().get('v.value');
        if(helper.actionMenuFunctionMap.hasOwnProperty(menuItemVaue)) {
            helper.actionMenuFunctionMap[menuItemVaue](component);
        }
    }
})