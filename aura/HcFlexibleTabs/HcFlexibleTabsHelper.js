({
    handleActive: function(component, event) {
        component.set('v.showSpinner', true);
        component.set("v.selectedTab", "none");
        var data = component.get("v.data");
        var tab = event.getSource();
        var tabId = tab.get("v.id");
        var componentName = "";
        if (tabId === "HcCarePlanBaseCmp") {
            component.set("v.selectedTab", "problemsAndGoals");
        } else if (tabId === "HcCareTeamTableContainer") {
            component.set("v.selectedTab", "careTeam");
        } else if (tabId === "HcFlexibleTask") {
            component.set("v.selectedTab", "flexibleTask");
        } else if (tabId === "HcGoalTableContainer") {
            component.set("v.selectedTab", "goals");
        }
        for (var i = 0; i < data.length; i++) {
            if (data[i].componentName === tabId) {
                var namespace = !$A.util.isUndefinedOrNull(data[i].namespace) ? data[i].namespace + ":" : "HealthCloudGA:";
                componentName = namespace + data[i].componentName;
            }
        }
        if (componentName.indexOf('none') == -1 && $A.util.isEmpty(tab.get('v.body'))) {
            var params = { carePlanId: component.get("v.carePlanId") };
            if (tabId === "HcCarePlanBaseCmp") {
                params.patientId = component.get("v.patientId");
            }
            if (tabId === "HcFlexibleTask") {
                params.selectedGroupName = component.getReference("v.selectedGroupName");
                params.actionButtonData = component.getReference("v.actionButtonData");
                params.actionMenuData = component.getReference("v.actionMenuData");
            }
            $A.createComponent(componentName, params,
                function(contentComponent, status, error) {
                    if (status === "SUCCESS") {
                        tab.set('v.body', contentComponent);
                    } else {
                        console.error('ERROR---->', error);
                    }
                    component.set('v.showSpinner', false);
                }
            );
        }
        component.set('v.showSpinner', false);
    },
    
    actionMenuFunctionMap: {
        ExpandTaskGroups: function(component) {
            var appEvent = $A.get("e.HealthCloudGA:HcFlexibleCarePlanAppEvent");
            appEvent.setParams({"carePlanId": component.get("v.carePlanId"), "groupType": component.get("v.selectedGroupName"), "action": "expandAll" });
            appEvent.fire();
        },
        CollapseTaskGroups: function(component) {
            var appEvent = $A.get("e.HealthCloudGA:HcFlexibleCarePlanAppEvent");
            appEvent.setParams({"carePlanId": component.get("v.carePlanId"), "groupType": component.get("v.selectedGroupName"), "action": "collapseAll" });
            appEvent.fire();
        },
        ApplyCarePlanTemplates: function(component) {
            HC.openCarePlanTemplatesSubtab( component.get("v.carePlanId"), 
                                            component.get("v.patientId"),
                                            HC.format($A.get("$Label.HealthCloudGA.Tab_Title_Careplan_Templates"), 
                                            component.get("v.carePlanSubject")));
        },
        AddCareTeamMember: function(component) {
            var caseId = component.get("v.carePlanId");
            $A.createComponent("HealthCloudGA:HcCareTeamAddMember", {
                    caseId: caseId
                },
                function(view, status, errorMessage) {
                    component.set("v.modalCmp", view);
                }
            );     
        }    
    },

    raiseSObjectEvent: function(component, eventParams) {
        var sObjectEvent = component.getEvent('sObjectEvent');
        sObjectEvent.setParams(eventParams);
        sObjectEvent.fire();
    },

    //TODO Siva : Refine
    resetModalComponent: function(component) {
        component.set('v.modalCmp', []);
    }
})