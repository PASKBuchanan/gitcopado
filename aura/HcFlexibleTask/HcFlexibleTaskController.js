({
    onInit: function(component, event, helper) {
        helper.getGroups(component, event);
        helper.processDateSection(component, event);
        helper.processNoGroupSection(component, event);
    },
    handleChange: function(component, event, helper) {
        // TODO - Siva: Cleanup in 214
        var selectedGroupName = Array.isArray(event.getParam('value')) ? event.getParam('value')[0] : event.getParam('value');
        component.set("v.actionButtonData", helper.getActionButtonData(selectedGroupName));
        component.set("v.actionMenuData", helper.getActionMenuData(selectedGroupName));
        component.set("v.selectedGroupName", selectedGroupName);
        helper.processSelectedGroup(component, event, selectedGroupName);
    },
    handleTaskStateFilterChange: function(component,event,helper) {
        // TODO - Siva: Cleanup in 214
        var taskState = Array.isArray(event.getParam('value')) ? event.getParam('value')[0] : event.getParam('value');
        component.set("v.currentTaskState",taskState);
        helper.raiseTaskStateFilterChangedEvent(component, event);
    },
    handleMessage: function(component, event, helper) {
        var evtType = event.getParam("type");
        if (evtType === 'taskGroupInitiated') {
            helper.raiseTaskStateFilterChangedEvent(component, event);
        }
        event.stopPropagation();
    }
})