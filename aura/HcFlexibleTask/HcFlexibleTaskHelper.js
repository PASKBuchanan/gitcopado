({
    getActionButtonData: function(labelKey) {
        var actionLabelMap = {
            Goal: '$Label.HealthCloudGA.Menu_Item_Create_New_Goal',
            Problem: '$Label.HealthCloudGA.Menu_Item_Create_New_Problem',
            Task: '$Label.HealthCloudGA.Menu_Item_Create_New_Task'
        };

        return actionLabelMap.hasOwnProperty(labelKey) ? { label: $A.get(actionLabelMap[labelKey]), value: labelKey } : { label: $A.get(actionLabelMap['Task']), value: 'Task' };
    },

    getActionMenuData: function(labelKey) {
        var defaultMenuItems = [{
            label: $A.get('$Label.HealthCloudGA.Text_Expand_All'),
            value: 'ExpandTaskGroups'
        }, {
            label: $A.get('$Label.HealthCloudGA.Text_Collapse_All'),
            value: 'CollapseTaskGroups'
        }];
        var menuItemsMap = {
            No_Grouping: []
        };

        return menuItemsMap.hasOwnProperty(labelKey) ? menuItemsMap[labelKey] : defaultMenuItems;
    },

    getGroups: function(component, event) {
        component.set("v.showSpinner", true);
        var defaultGroupByOptions = { 'Goal': '', 'Problem': '', 'No_Grouping': '', 'Date': '', 'Assigned_To': '' };
        var self = this;
        var defaultValue = '';
        var action = component.get("c.getTaskGroups");
        action.setStorable();
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                if (!$A.util.isEmpty(result)) {
                    defaultValue = result[0];
                    component.find('taskGroupFilter').set('v.value', defaultValue.GroupBy__c);
                    component.set("v.selectedGroupName", defaultValue.GroupBy__c);
                    component.set("v.actionButtonData", self.getActionButtonData(defaultValue.GroupBy__c));
                    component.set("v.actionMenuData", self.getActionMenuData(defaultValue.GroupBy__c));
                    var groups = [];
                    result.forEach(function(entry) {
                        if (defaultGroupByOptions.hasOwnProperty(entry.GroupBy__c)) {
                            var groupOption = {};
                            groupOption.label = entry.GroupByLabel__c;
                            groupOption.value = entry.GroupBy__c;
                            groups.push(groupOption);
                        }
                    });
                    self.getCustomLabelValue(component, groups);
                    self.processSelectedGroup(component, event, defaultValue.GroupBy__c);
                } else {
                    component.set("v.fcpNoGroupsMsg",$A.get("$Label.HealthCloudGA.Label_FCP_No_Groups"));
                }
                component.set("v.showSpinner", false);
            }
            if (state === "ERROR") {
                this.handleError(component, action.getError());
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
    },

    getCustomLabelValue: function(component, data) {
        var self = this;
        var count = 0;
        for (var ii = 0; ii < data.length; ii++) {
            $A.get(HC.getLabelIdFromUserText(data[ii].label), (function(i) {
                return function(label) {
                    if (!$A.util.isUndefinedOrNull(label) && label.indexOf('$Label.') == -1) {
                        data[i].label = label;
                    } else {
                        data[i].label = data[i].value;
                    }
                    count++;
                    if (count === data.length) {
                        component.set("v.groups", data);
                        self.getTaskStateFilterChange(component);
                    }
                };
            })(ii));
        }
    },

    getTaskStateFilterChange: function(component, event) {
        var taskStateFilters = [];
        var openOption = {};

        openOption.label = $A.get("$Label.HealthCloudGA.Title_Open_Cases");
        openOption.value = "Open";
        taskStateFilters.push(openOption);

        var closedOption = {};
        closedOption.label = $A.get("$Label.HealthCloudGA.Title_Closed_Cases");
        closedOption.value = "Closed";
        taskStateFilters.push(closedOption);

        var allOption = {};
        allOption.label = $A.get("$Label.HealthCloudGA.Title_All_Cases");
        allOption.value = "All";
        taskStateFilters.push(allOption);

        component.set("v.taskStateFilters", taskStateFilters);
        component.find('taskStateFilter').set('v.value', "Open");
    },

    processSelectedGroup: function(component, event, selectedGroupName) {
        if (selectedGroupName === 'Goal' && !component.get("v.goalSelected")) {
            component.set("v.goalSelected", true);
        } else if (selectedGroupName === 'Problem' && !component.get("v.problemSelected")) {
            component.set("v.problemSelected", true);
        } else if (selectedGroupName == 'Date') {
            component.set("v.dateSelected", true);
            this.raiseTaskStateFilterChangedEvent(component, event);
        } else if (selectedGroupName == 'No_Grouping') {
            component.set("v.noGroupSelected", true);
            this.raiseTaskStateFilterChangedEvent(component, event);
        } else if (selectedGroupName == 'Assigned_To') {
            component.set("v.ownerSelected", true);
        }
    },

    processDateSection: function(component, event) {
        var dateSections = [];
        var carePlanId = component.get("v.carePlanId");

        var noDueDate = { row: { Name: $A.get("$Label.HealthCloudGA.Header_Tasks_No_Due_Date"), Id: null, ActionName: 'getTasksWithNoDueDate', CarePlan__c: carePlanId, GroupType: 'Date' } };
        dateSections.push(noDueDate);

        var overDue = { row: { Name: $A.get("$Label.HealthCloudGA.Header_Overdue_Tasks"), Id: null, ActionName: 'getOverdueTasks', CarePlan__c: carePlanId, GroupType: 'Date' } };
        dateSections.push(overDue);

        var dueToday = { row: { Name: $A.get("$Label.HealthCloudGA.Header_Tasks_Due_Today"), Id: null, ActionName: 'getTasksDueToday', CarePlan__c: carePlanId, GroupType: 'Date' } };
        dateSections.push(dueToday);

        var dueTomorrow = { row: { Name: $A.get("$Label.HealthCloudGA.Header_Task_Due_Tomorrow"), Id: null, ActionName: 'getTasksDueTomorrow', CarePlan__c: carePlanId, GroupType: 'Date' } };
        dateSections.push(dueTomorrow);

        var dueBeyondTomorrow = { row: { Name: $A.get("$Label.HealthCloudGA.Header_Task_Due_Beyond_Tomorrow"), Id: null, ActionName: 'getTasksDue', CarePlan__c: carePlanId, GroupType: 'Date' } };
        dateSections.push(dueBeyondTomorrow);

        component.set("v.dateSections", dateSections);
    },

    processNoGroupSection: function(component, event) {
        var carePlanId = component.get("v.carePlanId");
        var noGroupSection = { row: { Name: $A.get("$Label.HealthCloudGA.Tab_Task"), Id: null, ActionName: 'getAllTasks', CarePlan__c: carePlanId, GroupType: 'No_Grouping' } };
        component.set("v.noGroupSection", noGroupSection);
    },

    raiseTaskStateFilterChangedEvent: function(component, event) {
        var taskState = component.get("v.currentTaskState");
        var appEvent = $A.get("e.HealthCloudGA:HcFlexibleCarePlanAppEvent");
        appEvent.setParams({
            carePlanId: component.get("v.carePlanId"),
            groupType: component.get("v.selectedGroupName"),
            action: "taskStateChange",
            actionData: {
                taskState: taskState
            }
        });
        appEvent.fire();
    }
})