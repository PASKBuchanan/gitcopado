({
    getData: function(actionName, component, event, helper, columnMeta) {
        var carePlanId = component.get("v.carePlanId");
        var carePlanIds = [carePlanId]
        var sObjectId = component.get("v.sObjectId");
        var sObjectIds = [sObjectId];

        var actionMap = {
            getTasksForGoals: "c.getTasksForGoals",
            getTasksForProblems: "c.getTasksForProblems",
            getOverdueTasks: "c.getOverdueTasks",
            getTasksDueToday: "c.getTasksDueToday",
            getTasksDueTomorrow: "c.getTasksDueTomorrow",
            getTasksDue: "c.getTasksDue",
            getTasksWithNoDueDate: "c.getTasksWithNoDueDate",
            getAllTasks: "c.getAllTasks",
            getTasksForOwner:"c.getTasksForOwner"
        };
        // temporarily hardcoding for All Open Closed
        var status = component.get("v.taskState");

        var actionParamsMap = {
            DEFAULT: {
                "carePlanIds": carePlanIds
            },
            getTasksForGoals: {
                "carePlanIds": carePlanIds,
                "goalIds": sObjectIds,
                "taskState": status
            },
            getTasksForProblems: {
                "carePlanIds": carePlanIds,
                "problemIds": sObjectIds,
                "taskState": status
            },

            getOverdueTasks: {
                "carePlanIds": carePlanIds,
                "taskState": status
            },

            getTasksDueToday: {
                "carePlanIds": carePlanIds,
                "taskState": status
            },

            getTasksDueTomorrow: {
                "carePlanIds": carePlanIds,
                "taskState": status
            },

            getTasksWithNoDueDate: {
                "carePlanIds": carePlanIds,
                "taskState": status
            },

            getAllTasks: {
                "carePlanIds": carePlanIds,
                "taskState": status
            },

            getTasksDue: {
                "carePlanIds": carePlanIds,
                "taskState": status
            },
            getTasksForOwner: {
                "carePlanIds": carePlanIds,
                "userIds": sObjectIds,
                "taskState": status
            }
        };
        var action = helper.getAction(actionName, actionMap, actionParamsMap, component, columnMeta);
        if ($A.util.isEmpty(action)) {
            var errorMsg = $A.get("$Label.HealthCloudGA.Msg_Invalid_Filter_Message");
            var compEvent = component.getEvent("error");
            compEvent.setParams({ error: errorMsg });
            compEvent.fire();
            return;
        }

        var noDataMessage = $A.get("$Label.HealthCloudGA.Text_No_Tasks");

        var xhrStart = new Date().getTime();
        action.setBackground();
        action.setCallback(this, function(response) {
            var xhrEnd = new Date().getTime();
            helper.progressMessage(' Xhr: ' + actionName + '#' + (xhrEnd - xhrStart), component.get('v.startT'));
            if (response.getState() == "SUCCESS") {
                var result = response.getReturnValue();
                var _resultList = result.recordsData;

                /* Any custom result manipulations */
                for (var i = 0; i < _resultList.length; i++) {
                    var task = _resultList[i];
                    if (!$A.util.isEmpty(task.Owner)) {
                        var currentOwner = task.Owner;
                        task.Owner = JSON.parse(currentOwner);
                    }
                    if (!$A.util.isEmpty(task.Who)) {
                        var currentOwner = task.Who;
                        task.Who = JSON.parse(currentOwner);
                    }
                }

                var processResult = helper.processResponsePageControl(component, columnMeta, result);
                helper.raiseDataFetchedEvent(component,
                    processResult.eventType,
                    processResult.colMetadata,
                    _resultList,
                    processResult.hasMoreData,
                    actionName,
                    noDataMessage);
            } else if (response.getState() === "ERROR") {
                // add exception handling
                var errorMsg = helper.getErrorMessage(response);
                var compEvent = component.getEvent("error");
                compEvent.setParams({ error: errorMsg });
                compEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
})