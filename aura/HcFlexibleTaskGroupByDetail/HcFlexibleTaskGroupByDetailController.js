({	
	onInit: function(component, event, helper){
		helper.getActionItems(component, event);
	},
	
	handleAction: function(component, event, helper) {
        helper.raiseActionMenuEvent(component, event.getParam('value'));
    },
    
    toggleIcon: function(component, event, helper) {
        var expanded = component.get("v.expanded");
        component.set("v.expanded", !expanded);
        helper.togglePanel(component);
    },

    handleTaskEvent: function(component, event, helper) {
        helper.raiseTaskSObjectEvent(component, event.getParams());
        event.stopPropagation();
    },

    handleFlexibleCarePlanEvent: function(component, event, helper) {       
        var action = event.getParam("action") ? event.getParam("action") : '';
        var eventGroupType = event.getParam("groupType") ? event.getParam("groupType").toUpperCase() : '';
        var itemRow = component.get("v.itemData").row;
        var itemRowGroupType = itemRow.hasOwnProperty('GroupType') ? itemRow.GroupType.toUpperCase() : '';
        var carePlanId = event.getParam("carePlanId");
        if (carePlanId === itemRow.CarePlan__c) {
            if (action === 'expandAll' && eventGroupType === itemRowGroupType) {
                component.set("v.expanded", true);
                helper.togglePanel(component);
            } else if (action === 'collapseAll' && eventGroupType === itemRowGroupType) {
                component.set("v.expanded", false);
            } else if(action === 'taskStateChange') {
                var actionData = event.getParam('actionData');
                var eventTaskState = $A.util.isUndefinedOrNull(actionData) ? 'Open' : actionData.taskState;
                var localTaskState = component.get('v.taskState');
                if( localTaskState !== eventTaskState ) {
                    component.set("v.taskState", eventTaskState);
                    component.set("v.firstTimeAccessed", true);            
                    helper.togglePanel(component);
                }
            }
        }
    },

    handleRefreshEvent: function(component, event, helper) {
        var carePlanId = component.get('v.itemData.row.CarePlan__c');
        var groupById = component.get('v.itemData.row.Id');
        var idToCheck = $A.util.isEmpty(groupById) ? carePlanId : groupById;
        
        var sObjectName = event.getParam('sObjectName').toUpperCase();
        var groupType = component.get('v.itemData.row.GroupType').toUpperCase();
        var recordIdsToRefresh = event.getParam('recordIdsToRefresh');
        if(recordIdsToRefresh.indexOf(idToCheck) !== -1) {
            if(sObjectName === 'TASK') {
                // Update tasks progress only for groups with valid groupById (ie., goal, problem or user id)
                if(!$A.util.isEmpty(groupById)) {
                    helper.updateTaskCounts(component, carePlanId, groupType, groupById);
                }
                component.set("v.firstTimeAccessed", true);            
                helper.togglePanel(component);
            } else if(sObjectName === groupType) {
                // Update header details only for groups with valid groupById (ie., goal, problem or user id)
                if(!$A.util.isEmpty(groupById)) {
                    var sObjectRecord = event.getParam('record');
                    helper.updateHeaderDetails(component, sObjectRecord);
                }
            }
        }
    }
})