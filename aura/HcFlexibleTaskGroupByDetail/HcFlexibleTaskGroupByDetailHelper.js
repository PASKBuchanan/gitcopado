({
	menuItemsMap: {
        PROBLEM: [
            {
                labelId: '$Label.HealthCloudGA.Menu_Item_Edit_Problem',
                value: function(component) {
                    return {
                        sObjectAction: "EDIT",
                        sObjectType: "Problem",
                        sObject: {
                            Id: component.get("v.itemData").row.Id,
                            CarePlan__c: component.get("v.itemData.row.CarePlan__c")
                        }
                    };
                }
            }
        ],
        GOAL: [
            {
                labelId: '$Label.HealthCloudGA.Menu_Item_Edit_Goal',
                value: function(component) {
                    return {
                        sObjectAction: "EDIT",
                        sObjectType: "Goal",
                        sObject: {
                            Id: component.get("v.itemData").row.Id,
                            CarePlanProblem__c: component.get('v.itemData.row.CarePlanProblem__c'),
                            CarePlan__c: component.get("v.itemData.row.CarePlan__c")
                        }
                    };
                }
            }
        ] 
    },
    
	getActionItems: function(component){
		var self = this;
        if(!$A.util.isEmpty(component.get("v.itemData.row.Id"))){
            var groupType = component.get("v.itemData.row.GroupType").toUpperCase();
            var generateMenuItems = function(menuItemKey) {
                var menuItemSpecs = [];
                if(self.menuItemsMap.hasOwnProperty(menuItemKey)) {
                    var menuItemsSpec = self.menuItemsMap[menuItemKey];
                    for(let ii=0; ii<menuItemsSpec.length; ii++) {
                        menuItemsSpec[ii].label = $A.get(menuItemsSpec[ii].labelId);
                        menuItemSpecs.push([
                            "lightning:menuItem",
                            menuItemsSpec[ii]
                        ]);                    
                    }
                }
                return menuItemSpecs;
            };
            if(!$A.util.isEmpty(groupType)){
                var menuItemsToCreate = generateMenuItems(groupType);
        
                $A.createComponents(menuItemsToCreate, function(menuItems, status, statusMessage) {
                    if (status == "SUCCESS") {
                        component.set("v.actionMenuItems", menuItems);
                    }
                });
            }
    		}
	},
	
	raiseActionMenuEvent: function(component, valueFunction) {
        if( typeof valueFunction == "function" ) {
            var sObjectEvent = component.getEvent("sObjectEvent");
            sObjectEvent.setParams(valueFunction(component));
            sObjectEvent.fire();
        }
    },
    
    setExpandedTable: function(component) {
        var itemData = component.get("v.itemData");
        if ($A.util.isUndefinedOrNull(itemData) || $A.util.isUndefinedOrNull(itemData.row)) {
            this.showToast(component, { message: $A.get("$Label.HealthCloudGA.Msg_Error_Unable_To_Retrive_CarePlan") }, true, 'warning');
        } else {
            var itemRow = itemData.row;
            var sObjectId = itemRow.Id;
            var carePlanId = itemRow.CarePlan__c;
            var actionName = itemRow.ActionName;
            var groupType = itemRow.GroupType;
            if ($A.util.isUndefinedOrNull(actionName)) {
                if (groupType === 'Assigned_To') {
                    actionName = 'getTasksForOwner';
                } else if (groupType === 'Goal') {
                    actionName = 'getTasksForGoals';
                } else if (groupType === 'Problem') {
                    actionName = 'getTasksForProblems';
                }
            }
            var taskTable = component.find("taskTable");
            component.set("v.firstTimeAccessed", false);
            taskTable.initializeTable(actionName);
        }
    },

    togglePanel: function(component) {
        var firstTimeAccessed = component.get("v.firstTimeAccessed");
        if (component.get("v.expanded")) {
            if (firstTimeAccessed) {
                this.setExpandedTable(component);
            }
        }
    },

    raiseTaskSObjectEvent: function(component, eventParams) {
        var sObjectEventParams = eventParams;
        var taskRecord = $A.util.isUndefinedOrNull(eventParams.sObject) ? {} : eventParams.sObject;
        var itemRow = component.get("v.itemData").row;
        
        if (itemRow.GroupType === "Assigned_To") {
            taskRecord.OwnerId = itemRow.Id;
        } else if (itemRow.GroupType === "Goal") {
            taskRecord.CarePlanProblem__c = $A.util.isEmpty(itemRow.CarePlanProblem__c) ? '' : itemRow.CarePlanProblem__c;
            taskRecord.CarePlanGoal__c = $A.util.isEmpty(itemRow.Id) ? '' : itemRow.Id;
        } else if (itemRow.GroupType === "Problem") {
            taskRecord.CarePlanProblem__c = $A.util.isEmpty(itemRow.Id) ? '' : itemRow.Id;
        }
        taskRecord.WhatId = component.get('v.itemData.row.CarePlan__c');

        sObjectEventParams.sObject = taskRecord;
        var sObjectEvent = component.getEvent("sObjectEvent");
        sObjectEvent.setParams(sObjectEventParams);
        sObjectEvent.fire();
    },

    updateTaskCounts: function(component, carePlanId, groupType, groupById) {
        var taskProgressActionMap = {
            GOAL: "c.getTaskProgressForGoal",
            PROBLEM: "c.getTaskProgressForProblem",
            ASSIGNED_TO: "c.getTaskProgressForOwner"
        };
        if(taskProgressActionMap.hasOwnProperty(groupType) && !$A.util.isEmpty(carePlanId)) {
            var action = component.get(taskProgressActionMap[groupType]);
            action.setParams({
                carePlanId: carePlanId,
                sObjectId: groupById
            });
            action.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                    var result = response.getReturnValue();
                    if(!isNaN(result.totalCount) && !isNaN(result.completedCount)) {
                        component.set('v.itemData.row.completedCount', result.completedCount);
                        component.set('v.itemData.row.totalCount', result.totalCount);
                    }
                } else {
                    console.error("Error fetching task progress information : " + response.getError());
                }
            });
            $A.enqueueAction(action);
        }
    },

    updateHeaderDetails: function(component, sObjectRecord) {
        var itemRow = component.get('v.itemData.row');
        for(let fieldName in sObjectRecord) {
            if(itemRow.hasOwnProperty(fieldName)) {
                itemRow[fieldName] = sObjectRecord[fieldName];
            }
        }
        component.set('v.itemData.row', itemRow);
    }

})