({
	handleRefreshEvent: function(component, event, helper) {
		var carePlanId = component.get('v.carePlanId');
		var dataProvider = component.get('v.dataProvider');
		var groupBySObjectName = dataProvider[0].get('v.providerType').toUpperCase();
		var sObjectName = event.getParam('sObjectName').toUpperCase();
		var recordIdsToRefresh = event.getParam('recordIdsToRefresh');
		if( sObjectName === groupBySObjectName && recordIdsToRefresh.indexOf(carePlanId) !== -1) {
			helper.fetchData(component);
		}
	}
})