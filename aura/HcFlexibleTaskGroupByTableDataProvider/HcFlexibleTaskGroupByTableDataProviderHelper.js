({
    getData: function(actionName, component, event, helper, columnMeta) {
        var carePlanId = component.get("v.carePlanId");
        var unassociatedTasksRowMap = {
            "Goal": {
                Id: 'TasksWithNoGoal',
                row: {
                    Name: $A.get("$Label.HealthCloudGA.Header_Tasks_No_Goal"),
                    Id: null,
                    ActionName: 'getTasksForGoals',
                    CarePlan__c: carePlanId,
                    GroupType: 'Goal'
                }
            },
            "Problem": {
                Id: 'TasksWithNoProblem',
                row: {
                    Name: $A.get("$Label.HealthCloudGA.Header_Tasks_No_Problem"),
                    Id: null,
                    ActionName: 'getTasksForProblems',
                    CarePlan__c: carePlanId,
                    GroupType: 'Problem'
                }
            }
        };

        var actionMapper = {
            "Goal": "getGoalsForPlan",
            "Problem": "getProblemsForCarePlan",
            "Assigned_To": "getTaskOwnersForCarePlan"
        };

        var providerType = component.get("v.providerType");
        actionName = actionMapper[providerType];

        var carePlanIds = [];
        carePlanIds.push(carePlanId)
        if ($A.util.isEmpty(actionName)) {
            actionName = 'getGoalsForPlan';
        }

        var actionMap = {};
        actionMap[actionName] = "c." + actionName;
        var actionParamsMap = {};
        if (actionName == 'getTaskOwnersForCarePlan') {
            actionParamsMap[actionName] = { "carePlanId": carePlanId };
        } else {
            actionParamsMap[actionName] = { "carePlanIds": carePlanIds };
        }
        var noDataMessage = $A.get("$Label.HealthCloudGA.Text_No_Results");

        var action = helper.getAction(actionName, actionMap, actionParamsMap, component, columnMeta);
        if ($A.util.isEmpty(action)) {
            var errorMsg = $A.get("$Label.HealthCloudGA.Msg_Invalid_Filter_Message");
            var compEvent = component.getEvent("error");
            compEvent.setParams({ error: errorMsg });
            compEvent.fire();
            return;
        }

        var xhrStart = new Date().getTime();
        action.setCallback(this, function(response) {
            var xhrEnd = new Date().getTime();
            helper.progressMessage(' Xhr: ' + actionName + '#' + (xhrEnd - xhrStart), component.get('v.startT'));
            if (response.getState() == "SUCCESS") {
                var result = response.getReturnValue();
                var _resultList = result.recordsData;

                var processResult = helper.processResponsePageControl(component, columnMeta, result);
                var panelMeta = [];
                var panelRow = component.get("v.currentPage") == 1 && unassociatedTasksRowMap.hasOwnProperty(providerType) ? [unassociatedTasksRowMap[providerType]] : [];

                for (var i = 0; i < _resultList.length; i++) {
                    _resultList[i].GroupType = providerType;
                    var resultRow = {
                        metadata: processResult.colMetadata,
                        row: _resultList[i],
                        Id: _resultList[i].Id
                    };
                    panelRow.push(resultRow);
                }

                helper.raiseDataFetchedEvent(component,
                    processResult.eventType,
                    panelMeta,
                    panelRow,
                    processResult.hasMoreData,
                    actionName,
                    noDataMessage);

                // This event lets parent Task component know that this task group is intialized.
                // With this information parent task component will pass down current task state to its children.
                var newTaskGroupEvent = component.getEvent("HcMessageCmpEvent");
                newTaskGroupEvent.setParams({
                    "type": "taskGroupInitiated",
                });
                newTaskGroupEvent.fire();
            } else if (response.getState() === "ERROR") {
                // add exception handling
                var errorMsg = helper.getErrorMessage(response);
                if (errorMsg.indexOf($A.get("$Label.HealthCloudGA.Msg_Component_Has_NoAccess")) != -1) {
                    component.set("v.isPSLEnforced", true);
                } else {
                    var compEvent = component.getEvent("error");
                    compEvent.setParams({ error: errorMsg });
                    compEvent.fire();
                }
            }
        });
        $A.enqueueAction(action);
    },
})