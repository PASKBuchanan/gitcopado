({
    doInit: function(component, event, helper) {
        var columnLinkFunctionMap = {
            Name: function(itemData) {
                HC.openRecordSubTab(itemData.Id, itemData.Name);
            }
        };

        component.set('v.columnLinkFunctionMap', columnLinkFunctionMap);
        component.set("v.showSpinner", true);
    },
    
    handleActionEvent: function(component, event, helper) {
        var actionEvent = component.get('v.actionEvent');
        var eventType = actionEvent.getParam('eventType');
        var eventSubType = actionEvent.getParam('eventSubType');

        if (eventType == 'ACTION' && eventSubType == 'EDIT') {
            helper.editGoal(component, actionEvent.getParam('data'));
        }
    },

    handleSearchChange: function(component, event, helper) {
        component.set("v.showSpinner", true);
        helper.fetchData(component);
    },

    handleRefreshEvent: function(component, event, helper) {
        var carePlanId = component.get('v.carePlanId');
        var sObjectName = event.getParam('sObjectName').toUpperCase();
        var recordIdsToRefresh = event.getParam('recordIdsToRefresh');
        if( sObjectName === 'GOAL' && recordIdsToRefresh.indexOf(carePlanId) !== -1) {
            helper.fetchData(component);
        }
    }
})