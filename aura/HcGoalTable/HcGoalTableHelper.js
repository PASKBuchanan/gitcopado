({
    editGoal: function(component, goalObject) {
        var sObjectEvent = component.getEvent("goalEvent");
        sObjectEvent.setParams({
            sObjectAction: "EDIT",
            sObjectType: "Goal",
            sObject: goalObject
        });
        sObjectEvent.fire();
    }
})