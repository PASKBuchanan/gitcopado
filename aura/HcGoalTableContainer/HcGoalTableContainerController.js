({
    onSearchList: function(component, event, helper) {
        var searchText = event.getSource().get('v.value');
        helper.typeAheadDelayExecute(function() {
            component.find('goalDataProvider').set('v.searchTerm', searchText);
        });
    },
    handleGoalEvent: function(component, event, helper) {
        helper.raiseGoalSObjectEvent(component, event.getParams());
        event.stopPropagation();
    },
})