({
    raiseGoalSObjectEvent: function(component, eventParams) {
        var sObjectEventParams = eventParams;
        var goalRecord = $A.util.isUndefinedOrNull(eventParams.sObject) ? {} : eventParams.sObject;
        sObjectEventParams.sObject = goalRecord;
        var sObjectEvent = component.getEvent("sObjectEvent");
        sObjectEvent.setParams(sObjectEventParams);
        sObjectEvent.fire();
    }
})