({
    setSelectedRecordType: function(component, event, helper) {
        var recordType = event.getSource().get('v.value');
        component.set('v.selectedRecordType', recordType);
        var CarePlanRTSelectionEvent = component.getEvent("CarePlanRTSelectionEvent");
        CarePlanRTSelectionEvent.setParams({
            "type": "CarePlanRecordTypeSelection",
            "payload": recordType
        });
        CarePlanRTSelectionEvent.fire();
    }
})