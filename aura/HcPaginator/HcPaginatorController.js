({
	previous : function(component, event, helper) {
		var pageNumber = component.get('v.pageNumber');
		component.set('v.pageNumber', (pageNumber - 1) < 0 ? 0 : pageNumber - 1);
	},
	next : function(component, event, helper) {
		var pageNumber = component.get('v.pageNumber');
		component.set('v.pageNumber', pageNumber + 1);
	},
	hasMorePagesChanged : function(component, event, helper) {
		var oldHasMorePages = event.getParam('oldValue');
		var newHasMorePages = event.getParam('value');
		if( oldHasMorePages == true && newHasMorePages == false ) {
			component.set('v.pageCount', component.get('v.pageNumber'));
		} 
	}
})