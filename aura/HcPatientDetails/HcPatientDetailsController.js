({
	init: function(component, event, helper) {
		if(component.get('v.hasLicense') === true) {
			helper.getPatientData(component, helper);
		}
	}
})