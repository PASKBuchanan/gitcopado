({
	init: function(component, event, helper) {
		if(component.get('v.hasLicense') === true) {
			helper.getProfilePicUrl(component);
	        helper.getRecordDetails(component);
	    }
	}
})