({
    updateProgress : function(component, event, helper) {
        var actualCount = component.get("v.actualCount");
        var totalCount = component.get("v.totalCount");
        var progress = 0;
        if(!$A.util.isEmpty(actualCount) && !$A.util.isEmpty(totalCount)
          && actualCount != 0 && totalCount != 0){
            progress = (actualCount/totalCount)*100;
            component.set("v.progress", progress);
        }
        var color;
        if(progress < 30 && progress > 0){
           color = "#F42508"; 
        } else if(progress < 70 && progress > 30){
           color = "#F49E08";  
        } else if(progress > 70){
           color = "#08F41D";     
        }
        var progressClass ="width:"+progress+"%; background:"+color;
        component.set("v.progressClass",progressClass);
    }
})