({
    getDefaultCellMarkupObject : function( value ) {
        return {
                element: 'aura:text',
                attributes: {
                    value: value
                }   
            };  
    },
    
    cellMarkupObjectGenerator : {

        SELECTION : function( itemData, cellMetadata, component ) {
            component.set('v.isSelectionCell', true);
            var selectionType = cellMetadata.selectionAttributes.type.toUpperCase();
            component.set('v.selectionType', selectionType);
            
            return {
                element: 'lightning:input',
                attributes: {
                    value: false,
                    type: selectionType == 'SINGLE' ? 'radio' : 'checkbox',
                    name: component.get('v.tableId'),
                    label: 'select-row',
                    variant: 'label-hidden',
                    class: 'slds-align--absolute-center',
                    onchange: component.get('c.handleSelectionEvent')
                }   
            };
        },

        DATE : function( itemData, cellMetadata, component ) {
            var dateValue = $A.util.isEmpty(itemData[cellMetadata.name]) ? "" : itemData[cellMetadata.name]+'T00:00:00Z';
            return {
                element: 'ui:outputDateTime', // ui:outputDate doesn't respect the date format passed, hence not used.
                attributes: {
                    value: dateValue,
                    format: component.get('v.userDateFormat'),
                    timezone: 'GMT'
                }   
            };
        },

        DATETIME : function( itemData, cellMetadata, component ) {
            return {
                element: 'ui:outputDateTime',
                attributes: {
                    value: itemData[cellMetadata.name],
                    format: component.get('v.userDateTimeFormat')
                }  
            };
        },

        PHONE : function( itemData, cellMetadata, component ) {
            return {
                element: 'ui:outputPhone',
                attributes: {
                    value: itemData[cellMetadata.name]
                }   
            };
        },

        REFERENCE : function( itemData, cellMetadata, component ) {
            var linkMarkupObject = {
                element: 'aura:text',
                attributes: {
                    value: itemData[cellMetadata.name]
                }   
            };
            if(cellMetadata.hasOwnProperty('lookupIdProperty')) {
                linkMarkupObject = {
                    element: "aura:html", 
                    attributes: {
                        tag: "a",
                        body: itemData[cellMetadata.name] || "",
                        HTMLAttributes: {
                            href: "javascript" + ":void(0);",
                            onclick: component.getReference('c.handleReferenceClick')
                        }
                    }
                };
            }
            return linkMarkupObject;
        },
        HTMLFORMATTEDTEXT : function( itemData, cellMetadata, component ) {
            return {
                element: 'lightning:formattedRichText',
                attributes: {
                    value: itemData[cellMetadata.name]
                }   
            };
        },
        HC_COMPOSITETEXT : function( itemData, cellMetadata, component ) {
            let compositeText = HC.parseJSON(itemData[cellMetadata.name]);
            return {
                element: HC.orgNamespace + ':HcCompositeText',
                attributes: {
                    textLabel: $A.util.isEmpty(compositeText) ? '' : compositeText.textLabel,
                    imageURL: $A.util.isEmpty(compositeText) ? '' : compositeText.imageURL
                }   
            };
        }
    },

	generateCellMarkupFromType : function( itemData, cellMetadata, component ) {
        var markupObject = this.getDefaultCellMarkupObject( '' );

        if( !$A.util.isEmpty(itemData) && !$A.util.isEmpty(cellMetadata) )
        {
            markupObject = this.getDefaultCellMarkupObject( itemData[cellMetadata.name] );
            if( this.cellMarkupObjectGenerator.hasOwnProperty( cellMetadata.type.toUpperCase() ) )
            {
                markupObject = this.cellMarkupObjectGenerator[ cellMetadata.type.toUpperCase() ]( itemData, cellMetadata, component );
            }
        }
        return markupObject;
    }
})