({
    editTask: function(component, taskObject) {
        var sObjectEvent = component.getEvent("taskEvent");
        sObjectEvent.setParams({
            fromWhere: component.get("v.parentFeature"),
            sObjectAction: "EDIT",
            sObjectType: "Task",
            sObject: taskObject
        });
        sObjectEvent.fire();
    },

    createTask: function(component) {
        var sObjectEvent = component.getEvent("taskEvent");
        sObjectEvent.setParams({
            fromWhere: component.get("v.parentFeature"),
            sObjectAction: "CREATE",
            sObjectType: "Task"
        });
        sObjectEvent.fire();
    }
})