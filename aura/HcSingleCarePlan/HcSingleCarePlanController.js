({
    doInit: function(component, event, helper) {
    	if(component.get('v.hasLicense') === true) {
        	helper.getCarePlanDetailsByPatient(component);
        }
    }
})