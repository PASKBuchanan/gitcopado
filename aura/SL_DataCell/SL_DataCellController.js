({
    doInit: function(component,event,helper) {
        var curFld = component.get('v.curFld');
        var curRec = component.get('v.curRec');
        var depth = component.get('v.depth');
        var spacing = [];
        for (var ii=0; ii < depth; ii++){
            spacing.push("1");
        }
        var output = spacing + curRec[curFld];
        component.set('v.cellVal', curRec[curFld]);
        component.set('v.depthList', spacing);
    }
})