({
    getHierarchy: function(component) {
        var recordId = component.get("v.recordId");
		var apiName = component.get("v.sObjectName");
        var pfMap = component.get("c.getFields");
        pfMap.setParams(
            {flds: component.get('v.fieldListString').split(","), objName: apiName}
        );
        
        pfMap.setCallback(this,function(result){
            component.set('v.fieldList', result.getReturnValue());  
            var retrieveHierarchy = component.get("c.getHierarchy");
            var recordId = component.get("v.recordId");
            retrieveHierarchy.setParams({sObjectId: recordId, lookupAPIName: component.get("v.lookupAPIName"), fieldsToQueryData: JSON.stringify(component.get("v.fieldList"))});
    
            retrieveHierarchy.setCallback(this,function(result) {
                var hierarchy=result.getReturnValue();
                component.set("v.rowWrapper", hierarchy);   
            });
            $A.enqueueAction(retrieveHierarchy);
        });
        $A.enqueueAction(pfMap);
    }
})