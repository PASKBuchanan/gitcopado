({

     init: function (component, event, helper) {
        try {
          component.set('v.state', JSON.stringify({
             configurationName: component.get('v.configurationName')
          }))
        } catch (e) {
            debugger;
        }
     },

     receiveMessage: function (component, event, helper) {
        var type = (event.getParam('type') || '').toLowerCase();
        var payload = event.getParam('payload') || {};
        switch (type) {
            case 'init':
              return;
            case 'apex':
              return helper.processRemoteAction(component, JSON.parse(JSON.stringify(payload)));
            case 'navigate':
              return helper.processNavigateAction(component, JSON.parse(JSON.stringify(payload)));
            case 'event':
              return payload.name && payload.params
              ? helper.fireAppEvent(payload.name, payload.params) 
              : false;
            case 'error':
              return helper.showToast(Object.assign(
                  { type: 'error', title: 'An Error Occured' }, 
                  JSON.parse(JSON.stringify(payload))
              ));
                
            default:
              console.error('ILLEGAL MESSAGE TYPE:', type);
        }

   }
})