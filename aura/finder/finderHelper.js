({

    processRemoteAction: function (component, action) {
        if (!action) {
          return this.sendError(component, 'missing argument: action');
        }
        if (!action.method) {
          return this.sendError(component, 'missing argument: action.method');
        }
        if (!action.succeed) {
          console.warn('remote call without succeed callback:', action);
        }
        if (!action.failed) {
          console.warn('remote call without error callback:', action);
        }

        var helper = this;

        return this.remote(component, action.method, action.params || {})
        .then($A.getCallback(function (response) {
            if (action.succeed) {
              helper.sendMessage(component, action.succeed, response)
            }
        }))
        .catch($A.getCallback(function (error) {
            helper.sendMessage(component, action.failed || 'error', { error: error })
        }))
    },

    sendError: function (component, message) {
        return this.sendMessage(component, 'error', { error: error })
    },

    sendMessage: function (component, type, payload) {
        //console.log('searchHelper.sendMessage()', type, payload);
        try {
          component.find('container').sendMessage(type, payload);
        } catch (e) {
            debugger;
        }
    },

    processNavigateAction: function (component, action) {
        try {
          //console.log('processNavigateAction', action);
          var helper = this;
          if (action.sObject) {
              var workspace = component.find('workspace');
              console.log('nav!', workspace);
              if (workspace && workspace.openTab && !workspace.isConsoleNavigation()) {
                return workspace.openTab({
                  url: '#/sObject/' + action.sObject + '/view',
                  focus: true
                })
                .then(function(response) {
                  workspaceAPI.focusTab({
                    tabId: response
                  });
                })
                .catch(function(error) {
                  console.log(error);
                  helper.navigateToRecordId(action.sObject)
                });
              } else {
                  helper.navigateToRecordId(action.sObject)
              }
          }
        } catch (e) {
            console.error('Error in navigation action: ' + action)
            debugger;
        }
    },

    navigateToRecordId: function (id) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": id,
        });
        navEvt.fire();
    },
    
    /**
     * @param [title]	{String}				title in bold
     * @param message {String}					message to display	
     * @param [messageTemplate] {String}		Overwrites message. Requires messageTemplateData.	
     * @param [messageTemplateData] {Object}	An array of text and actions to be used in messageTemplate.	
     * @param [key] {String}					Specifies an icon when the toast type is other. 
     *                                          Icon keys are available at the Lightning Design System Resources page.	
     * @param [duration=5000] {Integer}			Toast duration in milliseconds
     * @param [type='other'] {'other'|'error'|'warning'|'success'|'info'}	
                                                The default is other, which is styled like an info toast 
                                                and doesn’t display an icon, unless specified by the key attribute.	
     * @param [mode='dismissible'] {'dismissible','pester','sticky'}						
                                                dismissible: Remains visible until you press the close button 
                                                		or duration has elapsed, whichever comes first.
                                                pester: Remains visible until duration has elapsed. 
                                                		No close button is displayed.
                                                sticky: Remains visible until you press the close buttons.
    **/
    showToast: function (options) {
        return options ? this.fireAppEvent('force:showToast', options) : false;
    },
    
    fireAppEvent: function (name, options) {
        try {
            if (typeof name !== 'string') { return false }
        	var event = $A.get("e." + name);
        	if (!event) { return; }
            options && event.setParams(options);
        	event.fire();
        	return true;
        } catch (e) {
            return false;
        }
    },

    processError: function (component, payload) {
        component.find('notifications').showToast({
            title: "Something Went Wrong",
            message: payload && payload.message || '',
            variant: 'error',
            mode: 'dismissable'
        });
    }

})