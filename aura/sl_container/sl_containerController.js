({

    _init: function (component) {
        var url = [
            $A.get('$Resource.' + component.get('v.resource')),
            '/',
            component.get('v.path')
        ].join('');
        component.set('v.url', url);
    },

    _handleFrameLoaded: function (component, event, helper) {
        helper.listenMessages(component);
        var state = JSON.parse(component.get('v.state') || '{}');
        var dataToSend = Object.assign({
          url: helper.getUrlParameters()
        }, state);
        helper.sendMessage(component, 'init', dataToSend);
    },

    sendMessage: function (component, event, helper) {
        try {
          var params = event.getParam('arguments');
          helper.sendMessage(component, params.type, params.payload);
        } catch (e) {
            debugger;
        }
    }

})
s