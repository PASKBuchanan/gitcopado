({
  getElement: function (component, id) {
    return component.find(id).getElement();
  },

  listenMessages: function (component) {
    var helper = this;
    window.addEventListener('message', $A.getCallback(function (event) {
      helper.receiveMessage(component, event);
    }));
  },

  receiveMessage: function (component, event) {
    if (!component.isValid()) { return }
    var message = {};
    var parse = typeof event.data === 'string';

    try {
      message = parse ? JSON.parse(event.data) : event.data;
    } catch (e) {
      // ignore;
      return;
    }

    if (!message.type) {
      return;

    } else if (message.type === 'init') {
      component.set('v.origin', event.origin);
      component.set('v.useJSON', parse);

    } else if (message.type === 'ui') {
      //            if (message.payload.height) {
      //                this.setFrameHeight(component, message.payload.height);
      //            }
      return;

    } else if (message.name !== component.get('v.resource')) {
      return;
    }

    try {
      //console.log('aura hears: ', message.type, message.payload);
      var e = component.getEvent('onMessage');
      e.setParams(message);
      e.fire();
    } catch (e) {
      debugger;
    }
  },

  sendMessage: function (component, type, payload) {
    try {
      var frame = this.getElement(component, 'frame');
      //console.log('aura says: ', type, payload);
      var message = { name: component.get('v.resource'), type: type, payload: payload };
      var data = component.get('v.useJSON') ? JSON.stringify(message) : message;
      frame.contentWindow.postMessage(data, window.location.origin);
    } catch (e) {
      debugger;
    }
  },

  getUrlParameters: function () {
    var hashParamsString = window.location.hash.replace(/^[#/\w]*\?/, '');
    var searchParamsString = window.location.search.replace(/^[#/\w]*\?/, '');
    return []
      .concat(hashParamsString.split('&'))
      .concat(searchParamsString.split('&'))
      .reduce(function (results, str) {
          if (!str) { return results }
          var arr = str.split('=');
          var name = arr[0];
          var value = arr.length > 1 ? arr[1] : true;
          if (name) {
            results[name] = value;
          }
          return results;
      }, {});
  },

  setFrameHeight: function (component, height) {
    try {
      //console.log('setFrameHeight', height);
      var fitHeight;
      if (isNaN(height)) {
        fitHeight = height;
      } else {
        var maxHeight = this.getMaxContainerHeight(component);
        fitHeight = ((maxHeight && height > maxHeight) ? maxHeight : height) + 'px';
      }
      this.getElement(component, 'frame').style.height = fitHeight;
    } catch (e) {
      debugger;
    }
  },

  getMaxContainerHeight: function (component) {

    var isOverflowScroll = function (node) {
      var values = { auto: true, scroll: true };
      var style = getComputedStyle(node, null);
      var o = style.getPropertyValue("overflow");
      var oy = style.getPropertyValue("overflow-y");
      //console.log(node, o, oy, node.getBoundingClientRect().height);
      return values[o] || values[oy];
    }

    var isPositionAbsolute = function (node) {
      var style = getComputedStyle(node, null);
      var position = style.getPropertyValue('position');
      //console.log('position:', position);
      return position === 'absolute';
    }


    try {
      var element = component.find('container').getElement();

      var parent = findParent(isOverflowScroll, element);
      if (parent) {
        var height = absoluteParent.getBoundingClientRect().height;
        //console.log('overflow ->', height);
        if (height) {
          return height
        }
      } else {
        //console.log('NO overflows')
      }

      var absoluteParent = findParent(isPositionAbsolute, element);
      if (absoluteParent) {
        var height = absoluteParent.getBoundingClientRect().height;
        //console.log('absolute ->', height);
        if (height) {
          return height;
        }
      } else {
        //console.log('NO absolutes')
      }

      //component.set('v.maxHeight', parent.clientHeight);
    } catch (e) {
      debugger;
    }

    function findParent(filter, node) {
      return !node || node === document.body
        ? null
        : filter(node)
          ? node
          : findParent(filter, node.parentElement)
    }
  }

})