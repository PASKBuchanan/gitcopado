({
    remote: function (component, event, helper) {
        return helper.remote(component, event.getParam('method'), event.getParam('args'), event.getParam('options'));
    }
})