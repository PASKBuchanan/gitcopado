({

    logSuccessRemoteAction: function (component, actionName, params, result) {
        try {
            console.groupCollapsed('%c ' + component.getName() + '.' + actionName + '(' + JSON.stringify(Object.values(params)) + ')', 'color: #009067;font-weight:normal; background-color:#00906701');
            console.log(result);
            console.groupEnd();
        } catch (e) {}
    },

    logErrorRemoteAction: function (component, actionName, params, message) {
        try {
            console.group('%c' + component.getName() + '.' + actionName + '(' + JSON.stringify(Object.values(params)) + ')', 'color: #e24825;font-weight:normal');
            console.log(params);
            console.error(message);
            console.groupEnd();
        } catch (e) {}
    },

    getResponseErrorMessage: function (errors) {
        var result = '';
        for (var i in errors) {
            if (errors[i] && errors[i].message) {
              result += errors[i].message + '\n';
            }
        }
        return result || 'something went wrong';
    },

    remote: function (component, actionName, params, options) {
        params = params || {};
        var currentOptions = Object.assign({ json: true, cache: false }, options || {});
        var helper = this;
        return new Promise($A.getCallback(function (resolve, reject) {

            var action = component.get('c.' + actionName);

            if (action === undefined) {
                return reject(new Error(['component', component.getName(), 'has no action', actionName].join(' ')));
            }

            action.setParams(params);

            if (currentOptions.cache) {
              	action.setStorable();
            }

            action.setCallback(helper, function (response) {

				        if (response.getState() === "ERROR") {
                  	var result = response.getError();
                  	var message = this.getResponseErrorMessage(result);
                	  helper.logErrorRemoteAction(component, actionName, params, message);
                	  return reject(message);

              	} else if (response.getState() === "SUCCESS") {
                    var result = response.getReturnValue();
                    if (currentOptions.json && typeof result === 'string') {
                    try {
                        result = JSON.parse(result);
                        if (result && result.error) {
                            helper.logErrorRemoteAction(component, actionName, params, result.error);
                            return reject(result.error);
                        }
        						} catch (e) {
                    } // at least we tried
                }
    	            helper.logSuccessRemoteAction(component, actionName, params, result);
        	        return resolve(result);
            	}
            });

            $A.enqueueAction(action);

        }));

	},

    isInsideBuilder: function () {
        var site = $A.get('$Site');
        return site && site.context && (site.context.viewType === 'Editor');
    },

    _wait: function (delay) {
        return new Promise(function(resolve) {
            setTimeout(resolve, delay || 1)
        })
    }

})