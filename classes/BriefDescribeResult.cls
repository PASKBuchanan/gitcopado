public class BriefDescribeResult {
    
	@AuraEnabled
    public String label;
    
    @AuraEnabled
	public String labelPlural;
    
    @AuraEnabled
	public BriefFieldDescribe[] fields = new List<BriefFieldDescribe>();

    public BriefDescribeResult(String label, String labelPlural, BriefFieldDescribe[] fields) {
        this.label = label;
        this.labelPlural = labelPlural;
        this.fields = fields;
    }
    
}