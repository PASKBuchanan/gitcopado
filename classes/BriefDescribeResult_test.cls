@isTest
public with sharing class BriefDescribeResult_test {

    @isTest static void test_constructor () {
        BriefDescribeResult d = new BriefDescribeResult(
            'Test',
            'Tests',
            new BriefFieldDescribe[]{ new BriefFieldDescribe(Contact.fields.Name.getDescribe()) }
        );
        System.assertEquals('Test', d.label);
        System.assertEquals('Tests', d.labelPlural);
        System.assertEquals(1, d.fields.size());
    }

}