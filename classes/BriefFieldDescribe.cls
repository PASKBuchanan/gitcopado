public class BriefFieldDescribe {
    
    static private Set<String> RESTRICTED_TYPE = new Set<String>{ 
        DisplayType.ENCRYPTEDSTRING + '',
        DisplayType.DATACATEGORYGROUPREFERENCE + ''
    };
        
    static private Set<String> WITH_OPTIONS_TYPE = new Set<String>{ 
        DisplayType.PICKLIST + '',
        DisplayType.MULTIPICKLIST + '',
        DisplayType.COMBOBOX + ''
    };
    
    @AuraEnabled
    public String path;

    @AuraEnabled
    public String type;
    
    @AuraEnabled
    public String extraTypeInfo;
    
    @AuraEnabled
    public Boolean htmlFormatted;

    @AuraEnabled
    public Boolean sortable;

    @AuraEnabled
    public Boolean nameField;
    
    @AuraEnabled
    public Boolean filterable;
    
    @AuraEnabled
    public Boolean restricted;

    @AuraEnabled
    public String label;

    @AuraEnabled
    public String inlineHelpText;

    @AuraEnabled
    public String referenceTo;
    
    @AuraEnabled
    public String relationshipName;
        
    @AuraEnabled
    public Integer digits;
 
    public Map<String,String> picklistValues;
   
    public BriefFieldDescribe (String[] path, String[] labels, Schema.DescribeFieldResult describe) {
        assignDescribe(describe);
        this.path = String.join(path, '.');
        this.label = labels == null || labels.size() == 0 
            ? parseLabel(describe)
            : joinLabels(labels);
    }
    
    public BriefFieldDescribe (String path, String[] labels, Schema.DescribeFieldResult describe) {
        assignDescribe(describe);
        this.path = path;
        this.label = labels == null || labels.size() == 0
            ? parseLabel(describe)
            : joinLabels(labels);
    }
    
    
    public BriefFieldDescribe (FieldSetMember member, Schema.DescribeFieldResult describe) {
        assignDescribe(describe);
        this.path = member.getFieldPath();
        this.label = member.getLabel();
    }
    
    public BriefFieldDescribe (Schema.DescribeFieldResult describe) {
        assignDescribe(describe);
        this.path = describe.getName();
        this.label = parseLabel(describe);
    }
    
    private String parseLabel (Schema.DescribeFieldResult describe) {
        return describe.getLabel().replace(' ID', '');
    }
    
    private String joinLabels (String[] labels) {
        String[] results = new List<String>();
        for (String label: labels) {
            results.add(label.replace(' ID', ''));
        }
        return String.join(results, '. ');
    }
    
    private void assignDescribe (Schema.DescribeFieldResult describe) {

        try {
            String describeString = JSON.serialize(describe, true);
            Map<String, Object> plainDescribe = (Map<String, Object>) JSON.deserializeUntyped(describeString);
            if (plainDescribe.get('extraTypeInfo') != null) {
                extraTypeInfo = (String) plainDescribe.get('extraTypeInfo');
            }
        } catch (Exception e) {
            System.debug('ERROR:::::::::::::: ' + e.getMessage());
        }


        inlineHelpText = describe.getInlineHelpText();
        String displayType = describe.getType() + '';
        type = displayType.toLowerCase();


        htmlFormatted = describe.isHtmlFormatted();
        sortable = describe.isSortable();
        filterable = describe.isFilterable();
        restricted = !describe.isAccessible() || RESTRICTED_TYPE.contains(displayType);
        
        digits = describe.getDigits();
        nameField = describe.isNameField();
        
        Schema.sObjectType[] references = describe.getReferenceTo();
        if (references.size() > 0) {
            referenceTo = references[0].getDescribe().getName();
            relationshipName = describe.getRelationshipName();
        }
        
        if (WITH_OPTIONS_TYPE.contains(displayType)) {
            picklistValues = getPicklistMap(describe.getPicklistValues());
        }
        
    }

    private Map<String,String> getPicklistMap (Schema.PicklistEntry[] picklistEntries) {
        Map<String,String> result = new Map<String,String>();
        for (Schema.PicklistEntry entry: picklistEntries) {
            if (entry.isActive()) {
                result.put(entry.getValue(), entry.getLabel());
            }
        }
        return result;
    }

}