@isTest
public class BriefFieldDescribe_test {

    @isTest static void test_constructor_paths_labels_describe () {
        BriefFieldDescribe d = new BriefFieldDescribe(
            new String[]{ 'Owner','Name' },
            new String[]{ 'Owner','Name' },
            Account.fields.Name.getDescribe()
        );
        System.assertEquals('Owner.Name', d.path);
        System.assertEquals('Owner. Name', d.label);
        System.assertEquals(true, d.nameField);
        System.assertEquals('string', d.type);
    }

    @isTest static void test_constructor_describe () {
        BriefFieldDescribe d = new BriefFieldDescribe(
            Contact.fields.Name.getDescribe()
        );
        System.assertEquals('Name', d.path);
        System.assertEquals('Full Name', d.label);
        System.assertEquals(true, d.nameField);
        System.assertEquals('string', d.type);
    }

    @isTest static void test_constructor_describe_reference () {
        BriefFieldDescribe d = new BriefFieldDescribe(
            Contact.fields.OwnerId.getDescribe()
        );
        System.assertEquals('OwnerId', d.path);
        System.assertEquals('Owner', d.label);
        System.assertEquals('User', d.referenceTo);
        System.assertEquals('reference', d.type);
    }

}