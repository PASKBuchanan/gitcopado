global with sharing class Describer {

    private static final Integer DEFAULT_LIMIT = 15;
    private static final String FILTER_OPERATOR = 'AND';

    @AuraEnabled
    public static String getConfiguration (String configurationName) {
        try {
            Finder_Configuration__c configuration = selectConfigurationRecord(configurationName);
            if (configuration == null) {
                configuration = createNewConfiguration(configurationName);
            }
            DescribeSObjectResult describe = Finder_Configuration__c.SObjectType.getDescribe();
            DescribeSObjectResult fieldDescribe = Finder_Configuration_Field__c.SObjectType.getDescribe();
            return JSON.serialize(
                    new Map<String, Object>{
                        'sObject' => configuration.SObject__c,
                        'fields' => getConfigurationFields(configuration),
                        'configurable' => fieldDescribe.isCreateable() && fieldDescribe.isDeletable()
                    },
                    true
            );
        } catch (Exception e) {
            return respondWithErrorMessage(e.getMessage());
        }
    }

    final static Set<DisplayType> USE_LIKE = new Set<DisplayType>{ DisplayType.STRING, DisplayType.PHONE, DisplayType.EMAIL, DisplayType.COMBOBOX };
    final static Set<DisplayType> USE_EQUAL = new Set<DisplayType>{ DisplayType.PICKLIST, DisplayType.MULTIPICKLIST };

    private static String getWhereClauseByFieldDescribe (String path, Schema.DescribeFieldResult describe, String value) {
        if (String.isBlank(value)) {
            return null;
        }
        DisplayType type = describe.getType();
        if (USE_LIKE.contains(type)) {
            return path + ' LIKE \'%' + value + '%\'';
        }
        if (USE_EQUAL.contains(type)) {
            String[] results = new List<String>();
            for (String option: value.split('\\|')) {
                results.add(path + ' = \'' + option + '\'');
            }
            return String.join(results, ' ' + FILTER_OPERATOR + ' ');
        }
        return null;
    }

    @AuraEnabled
    public static String getRecords (
        String sObjectName,
        String[] fieldNames,
        Decimal recordsAmount,
        Decimal pageNumber,
        Boolean orderDesc,
        String orderByField,
        Map<String,String> filters
    ) {
        String selectQuery = '';
        String countQueryString = '';
        String whereStatement = '';
        try {
            System.debug('getRecords:');
            System.debug('sObjectName:' + sObjectName);
            System.debug(fieldNames);
            System.debug(recordsAmount);
            System.debug(pageNumber);
            SL_BriefDescribe brief = new SL_BriefDescribe(sObjectName);
            System.debug(brief.getName());


            if (filters != null) {
                String[] whereClauses = new List<String>();
                for (String fieldName: new List<String>(filters.keySet())) {
                    String filterValue = getWhereClauseByFieldDescribe(fieldName, brief.describeField(fieldName), filters.get(fieldName));
                    if (filterValue != null) {
                        whereClauses.add('(' + filterValue + ')');
                    }
                }
                if (whereClauses.size() > 0) {
                    whereStatement = 'WHERE ' + String.join(whereClauses, ' OR ');
                }
            }

            selectQuery = String.join(new List<String>{
                    'SELECT', String.join( fieldNames, ',' ),
                    'FROM', sObjectName,
                    whereStatement,
                    'ORDER BY', orderByField, (orderDesc ? 'DESC' : 'ASC'), 'NULLS LAST',
                    'LIMIT ' + recordsAmount
            }, ' ');

            Map<String,Object> resultMap = new Map<String,Object>{
                'page' => pageNumber
            };

            resultMap.put('filters', filters);
            resultMap.put('query', selectQuery);

            if (pageNumber == 1) {
                countQueryString = String.join(new List<String>{
                        'SELECT', 'COUNT()',
                        'FROM', sObjectName,
                        whereStatement
                }, ' ');
                resultMap.put('total', Database.countQuery(countQueryString));
            } else if (pageNumber > 1) {
                selectQuery = selectQuery + ' OFFSET ' + ( (pageNumber-1)*recordsAmount );
            }

            System.debug('----selectQuery='+selectQuery);
            resultMap.put('records', Database.query(selectQuery));

            return JSON.serialize(resultMap, true);

        } catch (Exception e) {
            return respondWithErrorMessage(e.getMessage() + ', queries: \n' + selectQuery + '\n' + countQueryString);
        }
    }

    @AuraEnabled
    public static String describeObjectFields (String sObjectName, String[] fieldNames) {
        if (String.isBlank(sObjectName)) {
            throw new AuraHandledException('missing sObjectName');
        }
        if (fieldNames == null || fieldNames.size() == 0) {
            throw new AuraHandledException('missing fieldNames');
        }
        try {
            SL_BriefDescribe brief = new SL_BriefDescribe(sObjectName);
            return JSON.serialize(brief.describeViewRecordFields(new List<String>(new Set<String>(fieldNames))), true);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String describeAllObjectFields (String sObjectName) {
        SL_BriefDescribe brief = new SL_BriefDescribe(sObjectName);
        return JSON.serialize(new Map<String,Object>{
            'sObject' => sObjectName,
            'label' => brief.describe.getLabel(),
            'labelPlural' => brief.describe.getLabelPlural(),
            'fields' => brief.describeAllAccessibleFields()
        }, true);
    }

    public static Finder_Configuration__c selectConfigurationRecord (String configurationName) {
        Finder_Configuration__c[] results = [SELECT SObject__c FROM Finder_Configuration__c WHERE Name = :configurationName LIMIT 1];
        return results.size() > 0 ? results[0] : null;
    }

    private static String[] getConfigurationFields (Finder_Configuration__c configuration) {
        Finder_Configuration_Field__c[] results = [SELECT Order__c, Path__c FROM Finder_Configuration_Field__c WHERE Configuration__c = :configuration.Id ORDER BY Order__c ASC NULLS LAST];
        Set<String> resultFields = new Set<String>(); // prevents fields duplicates
        for (Finder_Configuration_Field__c result: results) {
            resultFields.add(result.Path__c);
        }
        return new List<String>(resultFields);
    }

    @AuraEnabled
    public static String addField (String configurationName, String path, Decimal columnOrder) {
        try {
            Finder_Configuration__c cfg = selectConfigurationRecord(configurationName);

            if (configurationHasField(cfg.Id, path)) {
                return respondWithErrorMessage('The field "' + path + '" already exists in configuration');
            }

            Finder_Configuration_Field__c[] fieldsToMove = getFieldsAfter(cfg.Id, columnOrder);
            for (Finder_Configuration_Field__c f: fieldsToMove) {
                f.Order__c++;
            }
            update fieldsToMove;

            Finder_Configuration_Field__c f = new Finder_Configuration_Field__c(
                Configuration__c = cfg.Id,
                Order__c = columnOrder,
                Path__c = path
            );
            insert f;

            return getConfiguration(configurationName);
        } catch (Exception e) {
            return respondWithErrorMessage(e.getMessage());
        }
    }

    @AuraEnabled
    public static String removeField (String configurationName, String path) {
        try {
            String cfgId = (String)(selectConfigurationRecord(configurationName).Id);

            if (!configurationHasField(cfgId, path)) {
                return respondWithErrorMessage('The field "' + path + '" not exists in configuration');
            }

            // we're ignoring config dups, so here we might have a few records:
            Finder_Configuration_Field__c[] f = [
                    SELECT Id
                    FROM Finder_Configuration_Field__c
                    WHERE Configuration__c = :cfgId
                    AND Path__c = :path
            ];
            delete f;
            return getConfiguration(configurationName);

        } catch (Exception e) {
            return respondWithErrorMessage(e.getMessage());
        }
    }

    @AuraEnabled
    public static String reorderFields (String configurationName, Map<String, Decimal> orders) {
        String cfgId = (String)(selectConfigurationRecord(configurationName).Id);
        Set<String> paths = orders.keySet();
        Finder_Configuration_Field__c[] records = [
                SELECT Id,Path__c,Order__c
                FROM Finder_Configuration_Field__c
                WHERE Configuration__c = :cfgId
                AND Path__c IN :paths
        ];
        for (Finder_Configuration_Field__c record: records) {
            Decimal order = orders.get(record.Path__c);
            if (order != null) {
                record.Order__c = order;
            }
        }
        update records;
        return getConfiguration(configurationName);
    }

    private static Finder_Configuration_Field__c[] updateConfigurationOrder (String cfgId) {
        Finder_Configuration_Field__c[] records = [
                SELECT Id,Path__c,Order__c
                FROM Finder_Configuration_Field__c
                WHERE Configuration__c = :cfgId
                ORDER BY Order__c
        ];
        for (Integer i=0; i < records.size(); i++) {
            records[i].Order__c = i;
        }
        update records;
        return records;
    }

    private static Finder_Configuration_Field__c[] getFieldsAfter (String cfgId, Decimal columnOrder) {
        return [
            SELECT Id,Path__c,Order__c
            FROM Finder_Configuration_Field__c
            WHERE Configuration__c = :cfgId
            AND Order__c > :columnOrder
        ];
    }

    private static Boolean configurationHasField (Id cfgId, String fieldPath) {
        Finder_Configuration_Field__c[] results = [
                SELECT Id
                FROM Finder_Configuration_Field__c
                WHERE Configuration__c = :cfgId
                AND Path__c = :fieldPath
                LIMIT 1
        ];
        return results.size() > 0;
    }

    private static String respondWithErrorMessage (String message) {
        return JSON.serialize(new Map<String,String>{
            'error' => message
        }, true);
    }

    static private Finder_Configuration__c createNewConfiguration (String configurationName) {

        String sObjectName = configurationName.replaceAll('_\\d+$', '');
        SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);
        if (sObjectType == null) {
            throw new SL_BriefDescribe.BriefDescribeException('Invalid Object: ' + configurationName);
        }
        Finder_Configuration__c record = new Finder_Configuration__c(
            Name = configurationName,
            SObject__c = sObjectName
        );
        insert record;

        Finder_Configuration_Field__c field = new Finder_Configuration_Field__c(
            Configuration__c=record.Id,
            Order__c=0,
            Path__c='Name'
        );
        insert field;

        return record;

    }




}