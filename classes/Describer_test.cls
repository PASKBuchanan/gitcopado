@isTest
public with sharing class Describer_test {

    @isTest static void test_getRecords () {
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(
                Describer.getRecords(
                        'Contact',
                        new String[]{'Name','Email'},
                        5, // recordsAmount,
                        0, // pageNumber,
                        false, // orderDesc,
                        'Name', // orderByField,
                        null // filters
                )
        );
        System.assertEquals('"SELECT Name,Email FROM Contact  ORDER BY Name ASC NULLS LAST LIMIT 5"', JSON.serialize(result.get('query')));
    }

    @isTest static void test_getConfiguration () {
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(
                Describer.getConfiguration('Contact_0')
        );
        System.assertEquals('"Contact"', JSON.serialize(result.get('sObject')));
        System.assertEquals('["Name"]', JSON.serialize(result.get('fields')));
        System.assertEquals('true', JSON.serialize(result.get('configurable')));
    }

    @isTest static void test_getConfiguration_error () {
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(
                Describer.getConfiguration('InvalidName')
        );
        System.assert(result.containsKey('error'));
    }

    @isTest static void test_addField () {
        Describer.getConfiguration('Contact_0');
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(
                Describer.addField('Contact_0', 'Email', 2)
        );
        System.assertEquals('["Name","Email"]', JSON.serialize(result.get('fields')));
    }

    @isTest static void test_removeField () {
        Describer.getConfiguration('Contact_0');
        Describer.addField('Contact_0', 'Email', 2);
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(
                Describer.removeField('Contact_0', 'Name')
        );
        System.assertEquals('["Email"]', JSON.serialize(result.get('fields')));
    }

    @isTest static void test_reorderFields () {
        Describer.getConfiguration('Contact_0');
        Describer.addField('Contact_0', 'Email', 2);
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(
                Describer.reorderFields('Contact_0', new Map<String,Decimal>{
                        'Name' => 2,
                        'Email' => 1
                })
        );
        Object fields = result.get('fields');
        System.assertEquals('["Email","Name"]', JSON.serialize(fields));
    }

    @isTest static void test_describeObjectFields () {
        Object[] result = (Object[])JSON.deserializeUntyped(
                Describer.describeObjectFields('Contact', new String[]{'Name','Email'})
        );
        System.assertEquals(2, result.size());
    }

    @isTest static void test_describeAllObjectFields () {
        Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(
                Describer.describeAllObjectFields('Contact')
        );
        System.assert(result.containsKey('label'));
        System.assert(result.containsKey('fields'));
    }

}