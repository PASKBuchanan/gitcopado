public with sharing class SL_BriefDescribe {

    public Schema.DescribeSObjectResult describe;
    private Map<String, Schema.SObjectField> fieldsMap;
    private Map<String,Schema.DescribeFieldResult> referenceFieldsDescribeMap;

    public SL_BriefDescribe (String sObjectName) {
        describe = SL_BriefDescribe.describeObject(sObjectName);
    }

    public SL_BriefDescribe (Schema.SObjectType sObjectType) {
        describe = SL_BriefDescribe.describeObject(sObjectType);
    }

    public Boolean isAccessible () {
        return describe.isAccessible();
    }

    public String getName () {
        return describe.getName();
    }

    public Schema.SObjectType getType () {
        return describe.getSObjectType();
    }

    public String[] filterAccessibleFields (String[] fieldPaths) {
        String[] result = new List<String>();
        for (String fieldPath: fieldPaths) {
            try {
                Schema.DescribeFieldResult fieldDescribe = describeField(fieldPath);
                if (fieldDescribe.isAccessible()) {
                    result.add(fieldPath);
                }
            } catch (Exception e) {
                //TODO
            }
        }
        return result;
    }

    /**
     * Return brief field describes (for view record) by
     * @param {String[]} fieldPaths
     */
    public BriefFieldDescribe[] describeViewRecordFields (String[] fieldPaths) {
        BriefFieldDescribe[] results = new List<BriefFieldDescribe>();
        for (String fieldPath: new List<String>(new Set<String>(fieldPaths))) {
            DescribeFieldResult describe = describeField(fieldPath);
            if (describe.isAccessible()) {
                results.add(new BriefFieldDescribe(fieldPath, getFieldFullLabel(fieldPath), describe));
            }
        }
        return results;
    }

    /**
     * Return brief field describes (for view record) by
     * @param {String[]} fieldPaths
     */
    public Map<String,BriefFieldDescribe> describeViewRecordFieldsMap (String[] fieldPaths) {
        Map<String,BriefFieldDescribe> results = new Map<String,BriefFieldDescribe>();
        for (String fieldPath: fieldPaths) {
            DescribeFieldResult describe = describeField(fieldPath);
            if (describe.isAccessible()) {
                results.put(fieldPath, new BriefFieldDescribe(fieldPath, getFieldFullLabel(fieldPath), describe));
            }
        }
        return results;
    }

    public Schema.DescribeFieldResult describeField (String fieldPath) {
        Schema.DescribeFieldResult fieldDescribe = describeOwnField(fieldPath);
        if (fieldDescribe != null) {
            return fieldDescribe;
        }

        Schema.DescribeFieldResult refField = describeReferenceField(fieldPath);
        Schema.SObjectType refObject = refField.getReferenceTo()[0];
        SL_BriefDescribe refObjectBriefDescribe = new SL_BriefDescribe(refObject);
        String childPath = fieldPath.replaceFirst('^\\w+\\.', '');
        return refObjectBriefDescribe.describeField(childPath);
    }

    private Schema.DescribeFieldResult describeOwnField (String fieldPath) {
        return getFieldsMap().containsKey(fieldPath)
            ? getFieldsMap().get(fieldPath).getDescribe()
            : null;
    }

    private Schema.DescribeFieldResult describeReferenceField (String fieldPath) {
        if (referenceFieldsDescribeMap == null) {
            referenceFieldsDescribeMap = getReferenceFieldsMap();
        }
        if (!fieldPath.contains('.')) {
            throw new BriefDescribeException('Sorry, cannot parse path "' + fieldPath + '" in ' + describe.getName());
        }
        String referenceName = fieldPath.substring(0, fieldPath.indexOf('.'));
        if (!referenceFieldsDescribeMap.containsKey(referenceName) ) {
            throw new BriefDescribeException('Object ' + describe.getName() + ' has no relation "' + referenceName);
        }
        return referenceFieldsDescribeMap.get(referenceName);
    }

    private Map<String, Schema.SObjectField> getFieldsMap () {
        if (fieldsMap == null) {
            fieldsMap = describe.fields.getMap();
        }
        return fieldsMap;
    }

    /**
      * @returns a map where
      * keys are relation names, e.g.: Contact__r
      */
    private Map<String,Schema.DescribeFieldResult> getReferenceFieldsMap () {
        Map<String,Schema.DescribeFieldResult> resultMap = new Map<String,Schema.DescribeFieldResult>();
        for (Schema.SObjectField field: getFieldsMap().values()) {
            Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
            String relationshipName = fieldDescribe.getRelationshipName();
            if (relationshipName != null) {
                resultMap.put(relationshipName, fieldDescribe);
            }
        }
        return resultMap;
    }

    public String[] getFieldFullLabel (String path) {
        try {
            return getFieldFullLabel(path, new List<String>());
        } catch (Exception e) {
            throw new BriefDescribeException('cannot evaluate label for ' + path + ': ' + e.getMessage() + ' line ' + e.getLineNumber());
        }
    }
    public String[] getFieldFullLabel (String path, String[] parents) {
        if (String.isBlank(path)) {
            return parents;
        }
        Integer index = path.indexOf('.');
        String[] nextParents = new List<String>();
        nextParents.addAll(parents);

        if (index == -1) {
            // own field
            Schema.DescribeFieldResult field = describeOwnField(path);
            if (field != null) {
                nextParents.add(field.getLabel());
                return nextParents;
            }
        }

        Schema.DescribeFieldResult referenceField = describeReferenceField(path);
        if (referenceField != null) {
            // reference
            String refLabel = referenceField.getLabel().replaceAll('\\s*ID', '');
            nextParents.add(refLabel);
            SObjectType obj = referenceField.getReferenceTo()[0];
            SL_BriefDescribe refObjectBriefDescribe = new SL_BriefDescribe(obj);
            return refObjectBriefDescribe.getFieldFullLabel(path.replaceFirst('^\\w+\\.', ''), nextParents);
        }

        throw new BriefDescribeException('Field ' + path + ' not exists in ' + describe.getName());

    }

    public static Schema.DescribeSObjectResult describeObject(Schema.SObjectType sObjectType) {
        return sObjectType.getDescribe();
    }

    public static Schema.DescribeSObjectResult describeObject(String sObjectName) {
        Schema.DescribeSObjectResult[] results = Schema.describeSObjects(new List<String>{ sObjectName });
        if (results.size() == 0) {
            throw new BriefDescribeException('Unknown object: ' + sObjectName);
        }
        return results[0];
    }

    public BriefFieldDescribe[] describeAllAccessibleFields () {
        BriefFieldDescribe[] results = new List<BriefFieldDescribe>();
        for (SObjectField field: describe.fields.getMap().values()) {
            DescribeFieldResult fieldDescribe = field.getDescribe();
            if (fieldDescribe.isAccessible() && fieldDescribe.type != DisplayType.ENCRYPTEDSTRING && fieldDescribe.type != DisplayType.ID) {
                results.add(new BriefFieldDescribe(fieldDescribe));
            }
        }
        return results;
    }

    public class BriefDescribeException extends Exception {}

}