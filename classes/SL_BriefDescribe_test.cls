@isTest 
public class SL_BriefDescribe_test {

    @isTest static void test_createBySObjectName () {
        SL_BriefDescribe b = new SL_BriefDescribe('Account');
        System.assertEquals('Account', b.getName());
    }   
    
    @isTest static void test_createBySObjectType () {
        SL_BriefDescribe b = new SL_BriefDescribe(Account.SObjectType);
        System.assertEquals('Account', b.getName());
    }    
    
    @isTest static void test_isAccessible () {
        SL_BriefDescribe b = new SL_BriefDescribe(Account.SObjectType);
        System.assertEquals(Account.SObjectType.getDescribe().isAccessible(), b.isAccessible());
    }    

    @isTest static void test_getType () {
        SL_BriefDescribe b = new SL_BriefDescribe(Account.SObjectType);
        System.assertEquals(Account.SObjectType, b.getType());
    }    

    @isTest static void test_describeField () {
        SL_BriefDescribe b = new SL_BriefDescribe(Contact.SObjectType);
        System.assertEquals(Schema.SObjectType.Contact.fields.Name.getName(), b.describeField('Name').name);
        System.assertEquals(Schema.SObjectType.Account.fields.Name.getName(), b.describeField('Account.Name').name);
    }
    
    @isTest static void test_filterAccessibleFields () {
        SL_BriefDescribe b = new SL_BriefDescribe(Contact.SObjectType);
        b.filterAccessibleFields(new String[]{ '' });
    }

    @isTest static void test_getFieldFullLabel () {
        SL_BriefDescribe b = new SL_BriefDescribe(Contact.SObjectType);
        String label = String.join(b.getFieldFullLabel('CreatedBy.FirstName'), ' ');
        System.assertEquals('Created By First Name', label);
    }
    
    @isTest static void test_describeViewRecordFields () {
        SL_BriefDescribe b = new SL_BriefDescribe(Contact.SObjectType);
        BriefFieldDescribe[] results = b.describeViewRecordFields(new String[]{ 'Name', 'CreatedBy.FirstName' });
        System.assertEquals(2, results.size());
    }
    
    @isTest static void test_describeAllAccessibleFields () {
        SL_BriefDescribe b = new SL_BriefDescribe(Contact.SObjectType);
        BriefFieldDescribe[] results = b.describeAllAccessibleFields();
        System.assert(results.size() > 0);
    }

}