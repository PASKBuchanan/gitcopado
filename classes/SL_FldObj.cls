public class SL_FldObj
{
  @AuraEnabled
  public String label;
  @AuraEnabled
  public String apiName;
  @AuraEnabled
  public String fldType;
  @AuraEnabled
  public Boolean isRef;
  @AuraEnabled
  public String lkupRelAPI;
  @AuraEnabled
  public String lkupRelName;
  @AuraEnabled
  public String invalidField;
    
  public SL_FldObj(String apiName,
                   String label,
                   String fldType,
                   Boolean isRef,
                   String lkupRelAPI,
                   String lkupRelName,
                   String invalidField)
  {
    this.apiName = apiName;
    this.label = label;
    this.fldType = fldType;
    this.isRef = isRef;
    this.lkupRelAPI = lkupRelAPI;
    this.lkupRelName = lkupRelName;
    this.invalidField = invalidField; 
  }
}