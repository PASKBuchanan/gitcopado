public with sharing class SL_Hierarchy_Controller {
    private class Hierarchy implements Comparable{
        public SObject sObjectRow;
        public List<Hierarchy> children;
        public Integer depth;

        public Hierarchy (SObject sObjectRow, Integer depth) {
            this(sObjectRow, new List<Hierarchy>(), depth);
        }

        public Hierarchy (SObject sObjectRow, List<Hierarchy> children, Integer depth) {
            this.sObjectRow = sObjectRow;
            this.children = children;
            this.depth = depth;
        }

        public Integer compareTo(Object compareTo) {
            Hierarchy compareToHier = (Hierarchy)compareTo;
            Integer comparison = 0;
            if ( (String) sObjectRow.get('Name') < (String) compareToHier.sObjectRow.get('Name')) comparison = -1;
            else if ( (String) sObjectRow.get('Name') > (String) compareToHier.sObjectRow.get('Name')) comparison = 1;
            return comparison;  
        }
    }

    public class RowWrapper {
        @AuraEnabled 
        public SObject sObjectRow;
        @AuraEnabled 
        public Integer depth;

        public RowWrapper(Hierarchy hierarchy) {
            this.sObjectRow = hierarchy.sObjectRow;
            this.depth = hierarchy.depth;
        }
    }
    
    @AuraEnabled    
    public static SL_FldObj[] getFields(String[] flds, String objName) {
        Map<String, Schema.SObjectType> gdMap = Schema.getGlobalDescribe();
        Schema.Describesobjectresult rootObj = gdMap.get(objName).getDescribe();
        Map<String, Schema.SObjectField> rootFldMap = rootObj.fields.getMap();
        String strInvalidField = '';
        SL_FldObj[] fldMap = new SL_FldObj[]{};

        for(String key : flds) {
            key = rootFldMap.containsKey(key) ? rootFldMap.get(key).getDescribe().getName() : key; 
            String[] tempArry = key.split('\\.');

            if(tempArry.size() > 1) {
                Integer lstSize = tempArry.size();
                String finalFld = tempArry[lstSize-1].toLowerCase(); 
                
                Schema.Describesobjectresult finalObj;
                Schema.DescribeFieldResult finalFldRes;
                
                String[] actLabelArry = new String[]{};
                String actLabel = '';

                if(lstSize > 2) {
                    for(Integer i=0;i<=lstSize-2;i++) {
                        String tempFld = tempArry[i].toLowerCase();
        
                        if(tempFld.endsWith('__r')) {
                            tempFld = tempFld.removeEnd('__r') + '__c';
                        }
                        else {
                            tempFld += 'Id';
                        }
                        
                        strInvalidField = tempFld;
                        if(rootFldMap.containsKey(tempFld) || finalObj.fields.getMap().containsKey(tempFld)) {
                            strInvalidField = '';
                            if(i == 0) {
                                Schema.DescribeFieldResult dfr = rootFldMap.get(tempFld).getDescribe();
                                Schema.sObjectType[] refs = dfr.getReferenceTo();
                                
                                finalObj = refs[0].getDescribe();
                                actLabelArry.add(dfr.getLabel());
                            }
                            else {
                                Schema.DescribeFieldResult dfr = finalObj.fields.getMap().get(tempFld).getDescribe();
                                Schema.sObjectType[] refs = dfr.getReferenceTo();
                                
                                finalObj = refs[0].getDescribe();
                                actLabelArry.add(dfr.getLabel());
                            }
                        }
                    }
    
                    actLabel = String.join(actLabelArry,' > ');
                }
                else {
                    String tempFld = tempArry[0].toLowerCase();
    
                    if(tempFld.endsWith('__r')) {
                        tempFld = tempFld.removeEnd('__r') + '__c';
                    }
                    else {
                        tempFld += 'Id';
                    }
                    strInvalidField = tempFld;
                    if(rootFldMap.containsKey(tempFld)) {
                        strInvalidField = '';
                        Schema.DescribeFieldResult dfr = rootFldMap.get(tempFld).getDescribe();
                        Schema.sObjectType[] refs = dfr.getReferenceTo();
        
                        finalObj = refs[0].getDescribe();
                    }
                }

                Map<String, Schema.SObjectField> fieldMapTemp = finalObj.fields.getMap();
                strInvalidField = finalFld;
                if(fieldMapTemp.containsKey(finalFld)) {
                    strInvalidField = '';
                    Schema.DescribeFieldResult dfrTemp = fieldMapTemp.get(finalFld).getDescribe();
                    
                    actLabel += ((actLabel != '')?('.' + dfrTemp.getLabel()):(dfrTemp.getLabel()));
                    
                    String fldType = String.valueOf(dfrTemp.getType());
                    fldMap.add(createFldMap(key, actLabel, dfrTemp,strInvalidField));
                }
                else {
                    fldMap.add(createFldMap(key,'',null,strInvalidField)); 
                }
            }
            else {
                strInvalidField = key;
                if(rootFldMap.containsKey(key)) {
                    strInvalidField = '';
                    Schema.DescribeFieldResult dfr = rootFldMap.get(key).getDescribe();
                    fldMap.add(createFldMap(key,dfr.getLabel(),dfr,strInvalidField)); 
                }
                else {
                    fldMap.add(createFldMap(key,'',null,strInvalidField)); 
                }
            }
        }
        return fldMap;
    }
    
    private static SL_FldObj createFldMap(String apiName, String lbl, Schema.DescribeFieldResult dfr, String strInvalidField) {
       if(dfr != null) {
            String fldType = String.valueOf(dfr.getType());
            String lkupRelName = '';
            String lkupRelApiName = apiName;
            Boolean isRef;
    
            if(fldType == 'REFERENCE') {
                String[] tempArry = apiName.split('\\.');
                
                String[] relNameArry = new String[]{};
    
                if(tempArry.size()>2) {
                    for(Integer i=0;i<=tempArry.size()-2;i++) {
                        relNameArry.add(tempArry[i]);
                    }
    
                    lkupRelName = String.join(relNameArry,'.')+'.'+dfr.getRelationshipName()+'.Name';
                }
                else if(tempArry.size()==2) {
                    lkupRelName = tempArry[0]+'.'+dfr.getRelationshipName()+'.Name';
                }
                else {
                    lkupRelName = dfr.getRelationshipName()+'.Name';
                }
                isRef = true;
    
                // Id or Name should link to the record being displayed in the table
            } 
            else if(apiName == 'Id') {
                lkupRelName = apiName;
                isRef = true;
            } 
            else if(apiName == 'Name') {
                lkupRelName = apiName;
                lkupRelApiName = 'Id';
                isRef = true;
            } 
            else {
                isRef = false;
            }
            return new SL_FldObj(apiName,lbl,fldType,isRef,lkupRelApiName,lkupRelName,strInvalidField);
        }
        else {
            return new SL_FldObj(apiName,'','',false,'','',strInvalidField);            
        }
    }
    
    // accepts an Id and the lookup field with which to generate the hierarchy and returns the entire hierarchy
    @AuraEnabled 
    public static List<RowWrapper> getHierarchy(Id sObjectId, String lookupAPIName, String fieldsToQueryData) {
        DescribeSObjectResult objectDescription = sObjectId.getSObjectType().getDescribe();

        Map<String, Schema.SObjectField> fieldMap = objectDescription.fields.getMap();
        
        Schema.SObjectField lookupField = fieldMap.get(lookupAPIName);
        
        if (lookupField == null) return new List<RowWrapper>();
        
        Set<String> tempFields = new Set<String>();
        
        for (SL_FldObj field: (List<SL_FldObj>) JSON.deserialize(fieldsToQueryData, List<SL_FldObj>.class)) {
            String apiName = field.apiName;
            tempFields.add(apiName);
            Schema.SObjectField fieldToken = fieldMap.get(apiName);
            if (fieldToken != null) {
                Schema.DescribeFieldResult fieldDescribe = fieldToken.getDescribe();
                if (!fieldDescribe.getReferenceTo().isEmpty()) {
                    //Schema.sObjectType parentType = fieldDescribe.getReferenceTo().get(0);
                    tempFields.add(fieldDescribe.getRelationshipName() + '.Name');
                }
            }
        }

        tempFields.add(lookupAPIName);
        
        List<String> fieldsToQuery = new List<String>(tempFields);
        
        String relationshipName = lookupField.getDescribe().getRelationshipName();

        Set<String> relationshipFields = new Set<String>();
        String relationshipField = lookupAPIName;
        for (Integer ii = 0; ii < 5; ii++) {
            relationshipFields.add(relationshipField);
            relationshipField = relationshipName + '.' + relationshipField;
        }

        String query = 'SELECT Id, ' + String.join(new List<String>(relationshipFields), ',') + ' FROM ' + objectDescription.getName() + ' WHERE Id =: sObjectId';

        List<SObject> queriedRecords = Database.query(query);

        if (!queriedRecords.isEmpty()) {
            SObject targetRecord = queriedRecords[0];
            Id ultimateParent = (Id)
                (targetRecord.get(lookupAPIName) == null ? targetRecord.get('Id') :
                targetRecord.getSObject(relationshipName).get(lookupAPIName) == null ? targetRecord.get(lookupAPIName) :
                targetRecord.getSObject(relationshipName).getSObject(relationshipName).get(lookupAPIName) == null ? targetRecord.getSObject(relationshipName).get(lookupAPIName) :
                targetRecord.getSObject(relationshipName).getSObject(relationshipName).getSObject(relationshipName).get(lookupAPIName) == null ? targetRecord.getSObject(relationshipName).getSObject(relationshipName).get(lookupAPIName) :
                targetRecord.getSObject(relationshipName).getSObject(relationshipName).getSObject(relationshipName).getSObject(relationshipName).get(lookupAPIName) == null ? targetRecord.getSObject(relationshipName).getSObject(relationshipName).getSObject(relationshipName).get(lookupAPIName) :
                targetRecord.getSObject(relationshipName).getSObject(relationshipName).getSObject(relationshipName).getSObject(relationshipName).get(lookupAPIName));
            Map<Id, List<SObject>> parentsToChildren = new Map<Id, List<SObject>>();
            
            String queryList = '';
            if (!fieldsToQuery.isEmpty()) {
                queryList = String.join(fieldsToQuery, ',');
            }

            String finalQuery = 'SELECT Id, Name, ' + queryList + ' FROM ' + objectDescription.getName() + ' WHERE Id = : ultimateParent OR ' + String.join(new List<String>(relationshipFields), ' = :ultimateParent OR ') + ' = :ultimateParent';
            SObject ultimateRecord;
            for (SObject record : Database.query(finalQuery)) {
                if (ultimateParent == (Id) record.get('Id')) ultimateRecord = record;
                List<SObject> childrenRecords = parentsToChildren.get((String)record.get(lookupAPIName));
                if (childrenRecords == null) {
                    parentsToChildren.put((String)record.get(lookupAPIName), new List<SObject>{record});
                } 
                else {
                    childrenRecords.add(record);
                } 
            }
            return getFlattenedHierarchy(constructHierarchy(ultimateRecord, parentsToChildren, 0));
        }
        return null;    
    }

    private static List<RowWrapper> getFlattenedHierarchy(Hierarchy hierarchy) {
        List<RowWrapper> allRecords = new List<RowWrapper>();
        allRecords.add(new RowWrapper(hierarchy));

        if (hierarchy.children.isEmpty()) {
            return allRecords;
        }
        hierarchy.children.sort();
        for (Hierarchy childHierarchy : hierarchy.children) {
            allRecords.addAll(getFlattenedHierarchy(childHierarchy));
        }
        return allRecords;
    }

    private static Hierarchy constructHierarchy(SObject parent, Map<Id, List<SObject>> parentsToChildren, Integer depth) {
        List<SObject> childrenAccounts = parentsToChildren.get(parent.Id);
        if (childrenAccounts == null || childrenAccounts.isEmpty()) {
            return new Hierarchy(parent, depth);
        }
        List<Hierarchy> children = new List<Hierarchy>();
        for (SObject objAccount: childrenAccounts) {
            children.add(constructHierarchy(objAccount, parentsToChildren, depth + 1));
        }
        return new Hierarchy(parent, children, depth);
    }
}