@isTest
private class SL_Hierarchy_Test {
    
    @testSetup 
    private static void testSetup() {
        // insert accounts with parent accounts
        List<Account> accounts = new List<Account>{
            new Account(Name = 'Account1'),
            new Account(Name = 'Account2')
        };
        insert accounts;
        accounts[1].ParentId = accounts[0].Id;
        update accounts;
    }

    @isTest 
    private static void testHierarchy() {
        SL_Hierarchy_Controller.getHierarchy([SELECT Id FROM Account LIMIT 1].Id, 'ParentId', Json.serialize(SL_Hierarchy_Controller.getFields(new List<String>{'Industry', 'Type', 'AnnualRevenue', 'Owner.Name', 'ParentId'}, 'Account')));
    }
}