@isTest
private class SL_ctrl_DynTbl_Test {
	
	@isTest static void test_dynQry() {
	Group g = new Group(Name = 'Test SL_ctrl_DynTbl_Test group');
	insert g;

    User u = [select id from User where isactive=true limit 1];
	GroupMember o = new GroupMember(GroupId=g.Id, userorgroupid=u.id);
	insert o;

	Test.startTest();
	// Test without a given sort field or sort direction
	SL_ctrl_DynTbl.dynQry('Id,Name', 'Group', '', '', '', '', '', '', false);

	// Test with all parameters
		String jsonReturnVal = SL_ctrl_DynTbl.dynQry('Id,GroupId,UserOrGroupId,Group.CreatedDate,Group.CreatedBy.Name', 
												 'GroupMember', 
												 'GroupId', 
												 'DESC',
												 'ownerId=currentuser',
												 g.Id,
												 'GroupId', '', false);
	
	//test parent field sort
	SL_ctrl_DynTbl.dynQry('Id,GroupId,UserOrGroupId,Group.CreatedDate,Group.CreatedBy.Name', 
												 'GroupMember', 
												 'GroupId', 
												 'DESC',
												 'ownerId=current_user_contact',
												 g.Id,
												 'GroupId', 'RelatedId', true);	
	Test.stopTest();
	}
	
}