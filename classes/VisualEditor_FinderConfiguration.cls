global class VisualEditor_FinderConfiguration extends VisualEditor.DynamicPickList {
    
    VisualEditor.DesignTimePageContext context;
    
    global VisualEditor_FinderConfiguration (VisualEditor.DesignTimePageContext context) {
       this.context = context;
    }
    
    global override VisualEditor.DataRow getDefaultValue(){
        return new VisualEditor.DataRow('Contact', 'Contact');
    }

    global override VisualEditor.DynamicPickListRows getValues() {
        VisualEditor.DynamicPickListRows result = new VisualEditor.DynamicPickListRows();
        Finder_Configuration__c[] configs = getExistingConfigRecords();
        result.addAllRows(getRowsFromConfigurations(configs));
        result.addAllRows(getObjectRows(getConfigAmountPerSObject(configs)));
        return result;
    }

    static private VisualEditor.DataRow[] getObjectRows(Map<String,Integer> existingAmounts) {
        VisualEditor.DataRow[] results = new List<VisualEditor.DataRow>();
        for(Schema.SObjectType sObjectType: Schema.getGlobalDescribe().values()) {
            Schema.DescribeSObjectResult describe = sObjectType.getDescribe();
            String sObjectName = describe.getName();
            Integer nextIndex = existingAmounts.containsKey(sObjectName)
                ? existingAmounts.get(sObjectName)
                : 0;

            results.add(new VisualEditor.DataRow(describe.getLabel() + '*', describe.getName() + '_' + nextIndex));
        }
        return results;
    }

    static private String getSObjectLabel (String sObjectName) {
        Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);
        if (sObjectType != null) {
            return sObjectType.getDescribe().getLabel();
        }
        return null;
    }

    static private Finder_Configuration__c[] getExistingConfigRecords () {
        return [SELECT Name,SObject__c FROM Finder_Configuration__c];
    }

    static private Map<String,Integer> getConfigAmountPerSObject (Finder_Configuration__c[] configs) {
        Map<String,Integer> results = new Map<String,Integer>();
        for (Finder_Configuration__c cfg: configs) {
            if (!results.containsKey(cfg.SObject__c)) {
                results.put(cfg.SObject__c, 1);
            } else {
                results.put(cfg.SObject__c, results.get(cfg.SObject__c) + 1);
            }
        }
        return results;
    }

    static private VisualEditor.DataRow[] getRowsFromConfigurations (Finder_Configuration__c[] configs) {
        VisualEditor.DataRow[] results = new List<VisualEditor.DataRow>();
        for (Integer i=0; i < configs.size(); i++) {
            Finder_Configuration__c cfg = configs[i];

            String rowValue = cfg.Name;
            String rowLabel = getSObjectLabel(cfg.SObject__c);

            Integer amount = 0;
            for (VisualEditor.DataRow result: results) {
                if (result.getValue() == rowValue) {
                    amount++;
                }
            }
            if (amount > 0) {
                rowLabel += ' (' + (amount + 1) + ')';
            }
            results.add(new VisualEditor.DataRow(rowLabel, rowValue));
        }
        return results;
    }

}