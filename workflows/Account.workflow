<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Account_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>HealthCloudGA__IndustriesBusiness</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Account RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Lead Conversion</fullName>
        <actions>
            <name>Update_Account_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Lead_RT__c</field>
            <operation>equals</operation>
            <value>Sales_Leads</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
