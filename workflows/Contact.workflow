<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Contact_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Key_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Contact RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Lead Conversion %28Contact%29</fullName>
        <actions>
            <name>Update_Contact_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Lead_RT__c</field>
            <operation>equals</operation>
            <value>Sales_Leads</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
