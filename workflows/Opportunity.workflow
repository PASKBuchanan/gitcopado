<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>sl_Update_Stage_Date</fullName>
        <field>Start_Date_of_Current_Stage__c</field>
        <formula>TODAY ()</formula>
        <name>Update Stage Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Stage Date</fullName>
        <actions>
            <name>sl_Update_Stage_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( StageName )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
